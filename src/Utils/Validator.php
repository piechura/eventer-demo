<?php

namespace App\Utils;

use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Contracts\Translation\TranslatorInterface;
use function Symfony\Component\String\u;

class Validator
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function validateMinutes($minutes): int
    {
        if (empty($minutes)) {
            throw new InvalidArgumentException(
                $this->translator->trans('event_minutes.not_empty', [], 'validation')
            );
        }

        if (is_float($minutes)) {
            throw new InvalidArgumentException(
                $this->translator->trans('event_minutes.not_float', [], 'validation')
            );
        }
        if (!is_numeric($minutes)) {
            throw new InvalidArgumentException(
                $this->translator->trans('event_minutes.must_be_numeric', [], 'validation')
            );
        }
        return (int)$minutes;
    }

    public function validatePassword(?string $plainPassword): string
    {
        if (empty($plainPassword)) {
            throw new InvalidArgumentException(
                $this->translator->trans('user_password.not_empty', [], 'validation')
            );
        }

        if (u($plainPassword)->trim()->length() < 6) {
            throw new InvalidArgumentException(
                $this->translator->trans('user_password.min_long', [], 'validation')
            );
        }

        return $plainPassword;
    }

    public function validateEmail(?string $email): string
    {
        if (empty($email)) {
            throw new InvalidArgumentException(
                $this->translator->trans('user_email.not_empty', [], 'validation')
            );
        }

        if (null === u($email)->indexOf('@')) {
            throw new InvalidArgumentException(
                $this->translator->trans('user_email.real_email', [], 'validation')
            );
        }

        return $email;
    }

    public function validateFullName(?string $fullName): string
    {
        if (empty($fullName)) {
            throw new InvalidArgumentException(
                $this->translator->trans('user_full_name.not_empty', [], 'validation')
            );
        }

        return $fullName;
    }
}

<?php
namespace App\EventListener;

use App\Entity\Event;
use App\Entity\Mail;
use App\Service\ExceptionLoggerHandler;
use App\Service\Mailer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class EventEntitySubscriber implements EventSubscriber
{
    protected $twig;
    protected $em;
    protected $mailer;
    protected $translation;
    protected $router;
    protected $exceptionLoggerHandler;
    protected $security;

    public function __construct(
        Environment $twig,
        EntityManagerInterface $em,
        Mailer $mailer,
        TranslatorInterface $translation,
        UrlGeneratorInterface $router,
        ExceptionLoggerHandler $exceptionLoggerHandler,
        Security $security
    ) {
        $this->twig = $twig;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->translation = $translation;
        $this->router = $router;
        $this->exceptionLoggerHandler = $exceptionLoggerHandler;
        $this->security = $security;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof Event) {
            return;
        }
        $flush = false;
        $this->em = $args->getObjectManager();

        /*** clone if cycle and interval */
        if (!empty($entity->getCycle()) && !empty($entity->getInterval())) {
            $numberOfExtraAddedEvents = $entity->getCycle()-1;

            for ($i = 1; $i <= $numberOfExtraAddedEvents; $i++) {
                $this->em->persist($this->cloneEvent($entity));
            }
            $flush = true;
        }

        if ($entity->getAddedBy() === null) {
            $entity->setAddedBy($this->security->getUser());
            $flush = true;
        }

        if ($entity->getAddedBy()->isAdmin() || $entity->getAddedBy()->isKeeper()) {
            $entity->setAccepted(true);
            $this->confirmationMailWithAcceptanceNotification($entity);
            $flush = true;
        } else {
            $this->confirmationMailWithoutAcceptanceNotification($entity);
        }

        if ($flush) {
            $this->em->flush();
        }

    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof Event) {
            return;
        }
        $em = $args->getObjectManager();
        $uow = $em->getUnitOfWork();
        $updates = $uow->getEntityChangeSet($entity);

        $this->eventAcceptedMailNotification($updates, $entity);
    }


    private function cloneEvent($event) {
        $visitInterval = '+'.$event->getInterval().' days';

        $extraEvent = clone $event;
        $eventDateStart = $event->getStartAt();
        $eventDateEnd = $event->getEndAt();

        $eventDateStart = $eventDateStart->modify($visitInterval);
        $eventDateEnd = $eventDateEnd->modify($visitInterval);

        $extraEvent->setCycle(null);
        $extraEvent->setInterval(null);
        $extraEvent->setHash($extraEvent->generateHash());
        $extraEvent->setStartAt(new \DateTime($eventDateStart->format('Y-m-d H:i:s')));
        $extraEvent->setEndAt(new \DateTime($eventDateEnd->format('Y-m-d H:i:s')));
        $extraEvent->setAddedBy($this->security->getUser());
        return $extraEvent;
    }

    private function confirmationMailWithoutAcceptanceNotification(Event $event)
    {
        try {
            $email = [];
            $email['user'] = $event->getAddedBy();
            $email['toEmail'] = $event->getAddedBy()->getEmail();
            $email['type'] = Mail::EVENT_WITHOUT_ACCEPTANCE;
            $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::EVENT_WITHOUT_ACCEPTANCE) . ' - ' . $event->getName();
            $email['body'] = $this->twig->render(
                'email/simpleEmail.html.twig',
                [
                    'header' => $this->translation->trans('emails.event_without_acceptance_header'),
                    'text' => $this->translation->trans('emails.event_without_acceptance_text',
                        ['event_name' => $event->getName() . ' ' . $event->getStartAt()->format('Y-m-d H:i')]
                    ),
                    'buttonLabel' => $this->translation->trans('label.go_to_list'),
                    'buttonUrl' => $this->router->generate('panel_event_list', [], UrlGeneratorInterface::ABSOLUTE_URL),
                ]
            );
            $mailerStatus = $this->mailer->createMail($email);

            if ($mailerStatus !== true) {
                $this->exceptionLoggerHandler->handleException($mailerStatus);
            }
        } catch (\Exception $e) {
            $this->exceptionLoggerHandler->handleException($e);
        }
    }

    private function confirmationMailWithAcceptanceNotification(Event $event)
    {
        try {
            $email = [];
            $email['user'] = $event->getAddedBy();
            $email['toEmail'] = $event->getAddedBy()->getEmail();
            $email['type'] = Mail::EVENT_WITH_ACCEPTANCE;
            $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::EVENT_WITH_ACCEPTANCE) . ' - ' . $event->getName();
            $email['body'] = $this->twig->render(
                'email/simpleEmail.html.twig',
                [
                    'header' => $this->translation->trans(
                        'emails.event_with_acceptance_header'
                    ),
                    'text' => $this->translation->trans('emails.event_with_acceptance_text',
                        ['event_name' => $event->getName() . ' ' . $event->getStartAt()->format('Y-m-d H:i')]
                    ),
                ]
            );

            $mailerStatus = $this->mailer->createMail($email);

            if ($mailerStatus !== true) {
                $this->exceptionLoggerHandler->handleException($mailerStatus);
            }
        } catch (\Exception $e) {
            $this->exceptionLoggerHandler->handleException($e);
        }
    }

    private function eventAcceptedMailNotification($updates, Event $event)
    {
        if (array_key_exists('accepted', $updates) && $event->isAccepted() == true) {
            try {
                $email = [];
                $email['user'] = $event->getAddedBy();
                $email['toEmail'] = $event->getAddedBy()->getEmail();
                $email['type'] = Mail::EVENT_ACCEPTED;
                $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::EVENT_ACCEPTED) . ' - ' . $event->getName();
                $email['body'] = $this->twig->render(
                    'email/simpleEmail.html.twig',
                    [
                        'header' => $this->translation->trans('emails.event_accepted_header'),
                        'text' => $this->translation->trans('emails.event_accepted_text',
                            ['event_name' => $event->getName() . ' ' . $event->getStartAt()->format('Y-m-d H:i')]
                        ),
                    ]
                );
                $mailerStatus = $this->mailer->createMail($email);

                if ($mailerStatus !== true) {
                    $this->exceptionLoggerHandler->handleException($mailerStatus);
                }
            } catch (\Exception $e) {
                $this->exceptionLoggerHandler->handleException($e);
            }
        }
    }

}
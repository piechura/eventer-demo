<?php
namespace App\EventListener;

use App\Entity\Mail;
use App\Entity\User;
use App\Service\ExceptionLoggerHandler;
use App\Service\Mailer;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;

class UserEmailSubscriber implements EventSubscriber
{
    protected $twig;
    protected $em;
    protected $mailer;
    protected $translation;
    protected $router;
    protected $exceptionLoggerHandler;


    public function __construct(
        Environment $twig,
        EntityManagerInterface $em,
        Mailer $mailer,
        TranslatorInterface $translation,
        UrlGeneratorInterface $router,
        ExceptionLoggerHandler $exceptionLoggerHandler
    ) {
        $this->twig = $twig;
        $this->em = $em;
        $this->mailer = $mailer;
        $this->translation = $translation;
        $this->router = $router;
        $this->exceptionLoggerHandler = $exceptionLoggerHandler;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postPersist,
            Events::postUpdate,
        ];
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof User) {
            return;
        }
        $this->em = $args->getObjectManager();
        $this->activeTokenNotification($entity);
        $this->socialUserCreatedNotification($entity);
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if (!$entity instanceof User) {
            return;
        }
        $em = $args->getObjectManager();
        $uow = $em->getUnitOfWork();
        $updates = $uow->getEntityChangeSet($entity);

        $this->activationNotification($updates, $entity);
        $this->changePasswordNotification($updates, $entity);
        $this->changeEmailNotification($updates, $entity);
    }

    private function socialUserCreatedNotification(User $user)
    {
        if ($user->isSocialAuthenticated()) {
            $loginUrl = $this->router->generate('security_login', [],UrlGeneratorInterface::ABSOLUTE_URL);
            try {
                $email = [];
                $email['user'] = $user;
                $email['toEmail'] = $user->getEmail();
                $email['type'] = Mail::USER_SOCIAL_CREATED;
                $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::USER_SOCIAL_CREATED);
                $email['body'] = $this->twig->render(
                    'email/simpleEmail.html.twig',
                    [
                        'header' => $this->translation->trans('emails.user_social_header'),
                        'text' => $this->translation->trans('emails.user_social_text'),
                        'buttonLabel' => $this->translation->trans('emails.login'),
                        'buttonUrl' => $loginUrl,
                    ]
                );
                $mailerStatus = $this->mailer->createMail($email);
                if ($mailerStatus !== true) {
                    $this->exceptionLoggerHandler->handleException($mailerStatus);
                }
            } catch (\Exception $e) {
                $this->exceptionLoggerHandler->handleException($e);
            }
        }
    }

    private function activeTokenNotification(User $user)
    {
        if (!$user->isSocialAuthenticated()) {
            $user->setActiveToken(md5(random_bytes(10)));
            $this->em->flush();

            $activationUrl = $this->router->generate('user_activate_token',
                ['token' => $user->getActiveToken()],
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            try {
                $email = [];
                $email['user'] = $user;
                $email['toEmail'] = $user->getEmail();
                $email['type'] = Mail::USER_ACTIVATION_TOKEN;
                $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::USER_ACTIVATION_TOKEN);
                $email['body'] = $this->twig->render(
                    'email/simpleEmail.html.twig',
                    [
                        'header' => $this->translation->trans('emails.user_activation_token_email_header'),
                        'text' => $this->translation->trans('emails.user_activation_token_email_text', [
                            'inline_link_href' => $activationUrl
                        ]),
                        'buttonLabel' => $this->translation->trans('emails.click_here'),
                        'buttonUrl' => $activationUrl,
                    ]
                );
                $mailerStatus = $this->mailer->createMail($email);
                if ($mailerStatus !== true) {
                    $this->exceptionLoggerHandler->handleException($mailerStatus);
                }
            } catch (\Exception $e) {
                $this->exceptionLoggerHandler->handleException($e);
            }
        }
    }

    private function activationNotification($updates, User $user)
    {
        if (array_key_exists('isActive', $updates) && $user->isActive() == true) {
            try {
                $email = [];
                $email['user'] = $user;
                $email['toEmail'] = $user->getEmail();
                $email['type'] = Mail::USER_ACTIVATION;
                $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::USER_ACTIVATION);
                $email['body'] = $this->twig->render(
                    'email/simpleEmail.html.twig',
                    [
                        'header' => $this->translation->trans('emails.account_activated_header'),
                        'text' => $this->translation->trans('emails.account_activated_text'),
                        'buttonLabel' => $this->translation->trans('emails.login'),
                        'buttonUrl' => $this->router->generate('security_login', [], UrlGeneratorInterface::ABSOLUTE_URL),
                    ]
                );
                $mailerStatus = $this->mailer->createMail($email);
                if ($mailerStatus !== true) {
                    $this->exceptionLoggerHandler->handleException($mailerStatus);
                }
            } catch (\Exception $e) {
                $this->exceptionLoggerHandler->handleException($e);
            }
        }
    }

    private function changePasswordNotification($updates, User $user)
    {
        if (array_key_exists('password', $updates)) {
            try {
                $email = [];
                $email['user'] = $user;
                $email['toEmail'] = $user->getEmail();
                $email['type'] = Mail::USER_CHANGE_PASSWORD;
                $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::USER_CHANGE_PASSWORD);
                $email['body'] = $this->twig->render(
                    'email/simpleEmail.html.twig',
                    [
                        'header' => $this->translation->trans('emails.user_change_password_header'),
                        'text' => $this->translation->trans('emails.user_change_password_text'),
                        'buttonLabel' => $this->translation->trans('emails.login'),
                        'buttonUrl' => $this->router->generate('security_login', [], UrlGeneratorInterface::ABSOLUTE_URL),
                    ]
                );
                $mailerStatus = $this->mailer->createMail($email);

                if ($mailerStatus !== true) {
                    $this->exceptionLoggerHandler->handleException($mailerStatus);
                }
            } catch (\Exception $e) {
                $this->exceptionLoggerHandler->handleException($e);
            }
        }
    }

    private function changeEmailNotification($updates, User $user)
    {
        if (array_key_exists('email', $updates)) {
            try {
                $email = [];
                $email['user'] = $user;
                $email['toEmail'] = $user->getEmail();
                $email['type'] = Mail::USER_CHANGE_EMAIL;
                $email['subject'] = $this->translation->trans('emails.subjects.' . Mail::USER_CHANGE_EMAIL);
                $email['body'] = $this->twig->render(
                    'email/simpleEmail.html.twig',
                    [
                        'header' => $this->translation->trans('emails.user_change_email_header'),
                        'text' => $this->translation->trans('emails.user_change_email_text'),
                        'buttonLabel' => $this->translation->trans('emails.login'),
                        'buttonUrl' => $this->router->generate('security_login', [], UrlGeneratorInterface::ABSOLUTE_URL),
                    ]
                );
                $mailerStatus = $this->mailer->createMail($email);

                if ($mailerStatus !== true) {
                    $this->exceptionLoggerHandler->handleException($mailerStatus);
                }
            } catch (\Exception $e) {
                $this->exceptionLoggerHandler->handleException($e);
            }
        }
    }
}
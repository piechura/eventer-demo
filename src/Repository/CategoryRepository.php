<?php
namespace App\Repository;

use App\Entity\Category;
use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr\Join;
class CategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function getActiveCategories(): array
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.deletedAt IS NULL')
            ->orderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getActiveCategoriesChoices()
    {
        $categoryArray = [];
        /** @var Category $category */
        foreach ($this->getActiveCategories() as $category) {
            $categoryArray[$category->getId()] = $category->getName();
        }
        return $categoryArray;
    }

    public function findActiveCategories(): QueryBuilder
    {
        return $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.deletedAt IS NULL')
            ->orderBy('c.name', 'ASC');
    }

    public function getCategoriesWithEventsNumb(int $limit = 10): array
    {
        return $this->createQueryBuilder('c')
            ->select('c as entity, count(e.id) as count')
            ->where('c.deletedAt IS NULL')
            ->andWhere('e.deletedAt IS NULL')
            ->leftJoin(Event::class, 'e', Join::WITH, 'c.id = e.category')
            ->groupBy('c.id')
            ->orderBy('count', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}
<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;

class UserRepository extends ServiceEntityRepository implements UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getActiveUserCount(\DateTime $sinceDate = null): int
    {
        $query = $this->createQueryBuilder('u')
            ->select('count(u)')
            ->where('u.deletedAt IS NULL');

        if ($sinceDate) {
            $query
                ->andWhere('u.registerDate > :registerDate')
                ->setParameter('registerDate', $sinceDate->format('Y-m-d H:i:s'));
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    public function loadUserByUsername(string $email): ?User
    {
        return $this->createQueryBuilder('u')
                ->select('u')
                ->where('u.email = :email')
                ->setParameter('email', $email)
                ->getQuery()
                ->getOneOrNullResult();
    }

    public function getActiveUsers(): array
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.deletedAt IS NULL')
            ->orderBy('u.email', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getActiveUsersChoices(): array
    {
        $usersArray = [];
        /** @var User $user */
        foreach ($this->getActiveUsers() as $user) {
            $usersArray[$user->getId()] = $user->getEmail() . ' - ' . $user->getFullName();
        }
        return $usersArray;
    }

    public function getUserByActivationToken(string $activeToken): ?User
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.activeToken = :activeToken')
            ->setParameter('activeToken', $activeToken)
            ->getQuery()
            ->getSingleResult();
    }



}

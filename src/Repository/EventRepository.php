<?php
namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * @param int $minutes
     * @return int|mixed|string
     */
    public function findEventsByInterval(int $minutes)
    {
        $now = new \DateTime();
        $intervalStart = $now->modify('-' . $minutes . 'minutes');
        $intervalEnd =  new \DateTime();

        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.deletedAt IS NULL')
            ->andWhere('(e.startAt BETWEEN :intervalStart AND :intervalEnd)
            OR (e.endAt BETWEEN :intervalStart AND :intervalEnd)')
            ->setParameter('intervalStart', $intervalStart)
            ->setParameter('intervalEnd', $intervalEnd)
            ->getQuery()
            ->getResult();
    }

    public function getActiveEventCount(\DateTime $sinceDate = null, int $addedBy = null): int
    {
        $query = $this->createQueryBuilder('e')
            ->select('count(e)')
            ->where('e.deletedAt IS NULL');

        if ($sinceDate) {
            $query
                ->andWhere('e.createdAt > :createdAt')
                ->setParameter('createdAt', $sinceDate->format('Y-m-d H:i:s'));
        }

        if ($addedBy) {
            $query
                ->andWhere('e.addedBy = :addedBy')
                ->setParameter('addedBy', $addedBy);
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    public function findJoinedEventsByUser(int $userId, int $status)
    {
         return $this->createQueryBuilder('e')
            ->where('e.deletedAt IS NULL')
            ->innerJoin('e.participants', 'u')
            ->andWhere('e.status = :status')
            ->andWhere('u.id = :loggedUser')
            ->setParameter('loggedUser', $userId)
            ->setParameter('status', $status)
            ->getQuery()
            ->getResult();
    }


    public function findEventsBetweenDates(\DateTime $start, \DateTime $end, array $filters = [])
    {
        $query = $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.deletedAt IS NULL')
            ->andWhere('(e.startAt BETWEEN :start AND :end)
            OR (e.endAt BETWEEN :start AND :end)')
            ->setParameter('start', $start)
            ->setParameter('end', $end);

        if (isset($filters['category']) && $filters['category'] !== '') {
            $query
                ->andWhere('e.category = :category')
                ->setParameter('category', intval($filters['category']));
        }

        if (isset($filters['addedBy']) && $filters['addedBy'] !== '') {
            $query
                ->andWhere('e.addedBy = :addedBy')
                ->setParameter('addedBy', intval($filters['addedBy']));
        }

        if (isset($filters['entity']) && $filters['entity'] !== '') {
            $query
                ->andWhere('e.entity = :entity')
                ->setParameter('entity', intval($filters['entity']));
        }

        if (isset($filters['status']) && $filters['status'] !== '') {
            $query
                ->andWhere('e.status = :status')
                ->setParameter('status', intval($filters['status']));
        }
        if (isset($filters['accepted']) && $filters['accepted'] !== '') {
            $query
                ->andWhere('e.accepted = :accepted')
                ->setParameter('accepted', boolval($filters['accepted']));
        }
        $query->orderBy('e.startAt', 'ASC');

        return $query
            ->getQuery()
            ->getResult();
    }
}
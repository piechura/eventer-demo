<?php
namespace App\Repository;

use App\Entity\Entity;
use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\Expr\Join;

class EntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Entity::class);
    }

    public function getActiveEntities(): array
    {
        return $this->createQueryBuilder('e')
            ->select('e')
            ->where('e.deletedAt IS NULL')
            ->orderBy('e.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function getActiveEntitiesChoices(): array
    {
        $entityArray = [];
        /** @var Entity $entity */
        foreach ($this->getActiveEntities() as $entity) {
            $entityArray[$entity->getId()] = $entity->getName();
        }
        return $entityArray;
    }

    public function getActiveEntityCount(\DateTime $sinceDate = null, int $userId = null): int
    {
        $query = $this->createQueryBuilder('e')
            ->select('count(e)')
            ->where('e.deletedAt IS NULL');

        if ($sinceDate) {
            $query
                ->andWhere('e.createdAt > :createdAt')
                ->setParameter('createdAt', $sinceDate->format('Y-m-d H:i:s'));
        }

        if ($userId) {
            $query
                ->innerJoin('e.users', 'u')
                ->andWhere('u.id = :loggedUser')
                ->setParameter('loggedUser', $userId);
        }

        return $query->getQuery()->getSingleScalarResult();
    }

    public function getEntitiesWithEventsNumb(int $limit = 10): array
    {
        return $this->createQueryBuilder('e')
            ->select('e as entity, count(ev.id) as count')
            ->where('e.deletedAt IS NULL')
            ->andWhere('ev.deletedAt IS NULL')
            ->leftJoin(Event::class, 'ev', Join::WITH, 'e.id = ev.entity')
            ->groupBy('e.id')
            ->orderBy('count', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

}
<?php
namespace App\Repository;

use App\Entity\Mail;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

class MailRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Mail::class);
    }

    public function findNext($limit)
    {
        return $this->createQueryBuilder('m')
            ->select('m')
            ->where('m.deletedAt IS NULL')
            ->andWhere('m.isSent = :is_sent')
            ->andWhere('m.isFailed = :is_failed')
            ->setParameter('is_sent', 0)
            ->setParameter('is_failed', 0)
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }

    public function findUniqueMessagesTypes(): array
    {
        return $this->createQueryBuilder('m')
            ->select('m')
            ->where('m.deletedAt IS NULL')
            ->groupBy('m.type')
            ->getQuery()
            ->getResult();
    }

    public function findUniqueMessagesTypesChoices(): array
    {
        $typesArray = [];
        /** @var Mail $type */
        foreach ($this->findUniqueMessagesTypes() as $type) {
            $typesArray[$type->getType()] = $type->getType();
        }
        return $typesArray;
    }



}
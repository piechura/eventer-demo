<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\SoftDeletableInterface;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletableTrait;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @ORM\Table(name="user")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface, \Serializable, SoftDeletableInterface
{
    use SoftDeletableTrait;

    const INACTIVE = 0;
    const ACTIVE = 1;

    const ACTIVITY = [
        self::INACTIVE => 'status.inactive',
        self::ACTIVE => 'status.active',
    ];

    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_KEEPER = 'ROLE_KEEPER';
    const ROLE_USER = 'ROLE_USER';

    const ROLES = [
        self::ROLE_ADMIN => self::ROLE_ADMIN,
        self::ROLE_KEEPER => self::ROLE_KEEPER,
        self::ROLE_USER => self::ROLE_USER,
    ];

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Entity", mappedBy="users", cascade={"persist"})
     */
    private $entities;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Event", inversedBy="participants")
     * @ORM\OrderBy({"startAt" = "DESC"})
     */
    private $events;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     * @Assert\NotBlank()
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Limit: {{ limit }}"
     * )
     */
    private $fullName;

    /**
     * @ORM\Column(name="username", type="string", nullable=true)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(type="string", unique=true)
     * @Assert\Email()
     * @Assert\Length(
     *      max = 255,
     *      maxMessage = "Limit: {{ limit }}"
     * )
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $password;

    /**
     * @ORM\Column(name="active_token", type="string", nullable=true)
     */
    private $activeToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $expireToken;

    /**
     * @ORM\Column(name="is_active", type="boolean", nullable=true)
     */
    private $isActive;

    /**
     * @ORM\Column(type="datetime", nullable = true)
     */
    private $updateDate;

    /**
     * @ORM\Column(name="register_date", type="datetime")
     */
    private $registerDate;

    /**
     * @ORM\Column(name="last_login_date", type="datetime", nullable=true)
     */
    private $lastLoginDate;

    /**
     * @var array
     *
     * @ORM\Column(type="json")
     */
    private $roles = [];

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->isActive = false;
        $this->registerDate = new \DateTime();
        $this->entities = new ArrayCollection();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updateDate = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setFullName(string $fullName): void
    {
        $this->fullName = $fullName;
    }

    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    public function getUsername(): ?string
    {
        return $this->email;
    }

    public function setUsername(string $email): void
    {
        $this->email = $email;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getActiveToken()
    {
        return $this->activeToken;
    }

    /**
     * @param mixed $activeToken
     */
    public function setActiveToken($activeToken): void
    {
        $this->activeToken = $activeToken;
    }

    /**
     * @return mixed
     */
    public function getExpireToken()
    {
        return $this->expireToken;
    }

    /**
     * @param mixed $expireToken
     */
    public function setExpireToken($expireToken): void
    {
        $this->expireToken = $expireToken;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): void
    {
        $this->facebookId = $facebookId;
    }

    public function getGoogleId(): ?string
    {
        return $this->googleId;
    }

    public function setGoogleId(?string $googleId): void
    {
        $this->googleId = $googleId;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->isActive;
    }

    /**
     * @param bool $isActive
     */
    public function setIsActive(bool $isActive): void
    {
        $this->isActive = $isActive;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return \DateTime
     */
    public function getRegisterDate(): \DateTime
    {
        return $this->registerDate;
    }

    /**
     * @param \DateTime $registerDate
     */
    public function setRegisterDate(\DateTime $registerDate): void
    {
        $this->registerDate = $registerDate;
    }

    /**
     * Returns the roles or permissions granted to the user for security.
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantees that a user always has at least one role for security
        if (empty($roles)) {
            $roles[] = 'ROLE_USER';
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = $roles;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeletedAt(): ?DateTimeInterface
    {
        return $this->deletedAt;
    }

    /**
     * @param DateTimeInterface|null $deletedAt
     */
    public function setDeletedAt(?DateTimeInterface $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return mixed
     */
    public function getLastLoginDate()
    {
        return $this->lastLoginDate;
    }

    /**
     * @param mixed $lastLoginDate
     */
    public function setLastLoginDate($lastLoginDate): void
    {
        $this->lastLoginDate = $lastLoginDate;
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * {@inheritdoc}
     */
    public function getSalt(): ?string
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * {@inheritdoc}
     */
    public function eraseCredentials(): void
    {
        // if you had a plainPassword property, you'd nullify it here
        // $this->plainPassword = null;
    }

    /**
     * {@inheritdoc}
     */
    public function serialize(): string
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        return serialize([$this->id, $this->email, $this->password]);
    }

    /**
     * {@inheritdoc}
     */
    public function unserialize($serialized): void
    {
        // add $this->salt too if you don't use Bcrypt or Argon2i
        [$this->id, $this->email, $this->password] = unserialize($serialized, ['allowed_classes' => false]);
    }

    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        $this->events[] = $event;
        $event->addParticipant($this);
        return $this;
    }

    public function removeEvent(Event $event): self
    {
        $this->events->removeElement($event);
        $event->removeParticipant($this);
        return $this;
    }

    public function getEntities(): Collection
    {
        return $this->entities;
    }

    public function addEntity(Entity $entity): self
    {
        $this->entities[] = $entity;
        $entity->addUser($this);
        return $this;
    }

    public function removeEntity(Entity $entity): self
    {
        $this->entities->removeElement($entity);
        $entity->removeUser($this);
        return $this;
    }

    public function isSocialAuthenticated(): bool
    {
        if ($this->getGoogleId() || $this->getFacebookId()) {
            return true;
        }
        return false;
    }

    public function isAdmin(): bool
    {
        if (in_array(self::ROLE_ADMIN, $this->getRoles(), true)) {
            return true;
        }
        return false;
    }

    public function isKeeper(): bool
    {
        if (in_array(self::ROLE_KEEPER, $this->getRoles(), true)) {
            return true;
        }
        return false;
    }

    public function isUser(): bool
    {
        if (in_array(self::ROLE_USER, $this->getRoles(), true)) {
            return true;
        }
        return false;
    }


}

<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\SoftDeletableInterface;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletableTrait;

/**
* @ORM\Entity(repositoryClass="App\Repository\MailRepository")
* @ORM\HasLifecycleCallbacks
* @ORM\Table(name="mails")
*/
class Mail implements SoftDeletableInterface
{
    use SoftDeletableTrait;

    const CUSTOM_EMAIL = 'custom_email';
    const USER_ACTIVATION = 'user_activation';
    const USER_ACTIVATION_TOKEN = 'user_activation_token';
    const USER_SOCIAL_CREATED = 'user_social_created';
    const USER_CHANGE_PASSWORD = 'user_change_password';
    const USER_CHANGE_EMAIL = 'user_change_email';
    const EVENT_WITHOUT_ACCEPTANCE = 'event_without_acceptance';
    const EVENT_WITH_ACCEPTANCE = 'event_with_acceptance';
    const EVENT_ACCEPTED = 'event_accepted';

    const SUBJECTS = [
        self::CUSTOM_EMAIL,
        self::USER_ACTIVATION,
        self::USER_ACTIVATION_TOKEN,
        self::USER_SOCIAL_CREATED,
        self::USER_CHANGE_PASSWORD,
        self::USER_CHANGE_EMAIL,
        self::EVENT_WITH_ACCEPTANCE,
        self::EVENT_WITHOUT_ACCEPTANCE,
        self::EVENT_ACCEPTED,
    ];

    const STATUS = [
        0 => 'status.notSent',
        1 => 'status.sent'
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="from_email", type="string", length=255, nullable=false)
     */
    private $fromEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="from_name", type="string", length=255, nullable=false)
     */
    private $fromName;

    /**
     * @var string
     *
     * @ORM\Column(name="to_email", type="string", length=255, nullable=false)
     */
    private $toEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="text", length=255, nullable=false)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="text", nullable=false)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="alt_body", type="text", nullable=true)
     */
    private $altBody;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_sent", type="boolean", nullable=true)
     */
    private $isSent;

    /**
     * @var bool|null
     *
     * @ORM\Column(name="is_failed", type="boolean", nullable=true)
     */
    private $isFailed;
    /**
     * @ORM\Column(type="string", nullable=true, length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $files;


    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->isSent = false;
        $this->isFailed = false;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * Set fromEmail.
     *
     * @param string $fromEmail
     *
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * Get fromEmail.
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * @param $fromName
     * @return $this
     */
    public function setFromName($fromName)
    {
        $this->fromName = $fromName;

        return $this;
    }

    /**
     * Get fromName.
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->fromName;
    }

    /**
     * @param $toEmail
     * @return $this
     */
    public function setToEmail($toEmail)
    {
        $this->toEmail = $toEmail;
        return $this;
    }

    /**
     * Get toEmail.
     *
     * @return string
     */
    public function getToEmail()
    {
        return $this->toEmail;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;
        return $this;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param $altBody
     * @return $this
     */
    public function setAltBody($altBody)
    {
        $this->altBody = $altBody;
        return $this;
    }

    /**
     * @return string
     */
    public function getAltBody()
    {
        return $this->altBody;
    }

    /**
     * @param $createdAt
     * @return $this
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param $sentAt
     * @return $this
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * @param $isSent
     * @return $this
     */
    public function setIsSent($isSent)
    {
        $this->isSent = $isSent;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsSent()
    {
        return $this->isSent;
    }

    /**
     * @param $isFailed
     * @return $this
     */
    public function setIsFailed($isFailed)
    {
        $this->isFailed = $isFailed;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getIsFailed()
    {
        return $this->isFailed;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @param array $file
     */
    public function setFiles(array $files)
    {
        $this->files = $files;
    }
    /**
     * @param mixed $file
     */
    public function addFile($file)
    {
        $this->files[] = $file;
    }
}

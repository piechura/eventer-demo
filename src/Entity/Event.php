<?php
namespace App\Entity;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Knp\DoctrineBehaviors\Contract\Entity\SoftDeletableInterface;
use Knp\DoctrineBehaviors\Model\SoftDeletable\SoftDeletableTrait;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use App\Validator\Constraints as AppValidator;
use Doctrine\Common\Collections\Collection;

/**
* @ORM\Entity(repositoryClass="App\Repository\EventRepository")
* @ORM\HasLifecycleCallbacks
* @ORM\Table(name="event")
* @Vich\Uploadable
* @AppValidator\EventAddress
* @AppValidator\EventIntervalCycles
* @AppValidator\EventTime()
* @AppValidator\EventEntity()
*/
class Event implements SoftDeletableInterface
{
    use SoftDeletableTrait;

    const INCOMING = 0;
    const ONGOING = 1;
    const PAST = 2;

    const STATUS = [
        self::INCOMING => 'label.incoming',
        self::ONGOING => 'label.ongoing',
        self::PAST => 'label.past',
    ];

    const CYCLE_VISIT = [
        2 => 'cycles.two',
        3 => 'cycles.three',
        4 => 'cycles.four',
        5 => 'cycles.five'
    ];

    const INTERVAL_VISIT = [
        1 => 'intervals.one_day',
        2 => 'intervals.two_day',
        3 => 'intervals.three_day',
        4 => 'intervals.four_day',
        5 => 'intervals.five_day',
        6 => 'intervals.six_day',
        7 => 'intervals.seven_day'
    ];

    const ACCEPTED = [
        1 => 'label.accepted',
        0 => 'label.not_accepted',
    ];

    const IS_ENTITY = [
        1 => 'label.existing',
        0 => 'label.not_existing',
    ];
    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category")
     * @Assert\NotBlank()
     */
    private $category;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\User", mappedBy="events", cascade={"remove"})
     */
    private $participants;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Entity", cascade={"persist"})
     */
    private $entity;

    private $newEntity;

    private $isExistingEntity;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     */
    private $addedBy;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=100, nullable=false, unique=true)
     */
    private $hash;

    /**
     * @var string
     * @ORM\Column(type="text", length=150, nullable=false)
     * @Assert\Length(
     *      min = 5,
     *      max = 150,
     *      minMessage = "entity_validation.min",
     *      maxMessage = "entity_validation.max"
     * )
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="text")
     * @Assert\Length(
     *      max = 10000,
     *      maxMessage = "entity_validation.limit"
     * )
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     * @Assert\Length(
     *      max = 10000,
     *      maxMessage = "entity_validation.limit"
     * )
     */
    private $agenda;

    /**
     * @var string
     * @ORM\Column(type="text", length=100, nullable=true)
     * @Assert\Length(
     *      max = 100,
     *      maxMessage = "entity_validation.limit"
     * )
     */
    private $city;

    /**
     * @var string
     * @ORM\Column(type="text", length=200, nullable=true)
     * @Assert\Length(
     *      max = 200,
     *      maxMessage = "entity_validation.limit"
     * )
     */
    private $address;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $entityAddress;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotBlank()
     */
    private $startAt;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     * @Assert\NotBlank()
     */
    private $endAt;

    /**
     * @var string
     * @ORM\Column(type="text", length=100, nullable=true)
     */
    private $externalLink;

    /**
     * @ORM\Column(type="text", length=1000, nullable=true)
     * @Assert\Length(
     *      max = 1000,
     *      maxMessage = "entity_validation.limit"
     * )
     */
    private $tickets;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $freeEntry;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $accepted;

    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $cycleInterval;

    /**
     * @var string
     * @ORM\Column(type="text", length=255, nullable=true)
     */
    private $imageName;

    /**
     * @Vich\UploadableField(mapping="event_image", fileNameProperty="imageName", size="eventImageSize")
     *
     * @var File|null
     */
    private $imageFile;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $eventImageSize;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updateDate;

    private $interval;
    private $cycle;

    public function __construct() {
        $this->createdAt = new \DateTime();
        $this->hash = $this->generateHash();
        $this->status = self::INCOMING;
        $this->accepted = false;
        $this->entityAddress = true;
        $this->isExistingEntity = true;
        $this->participants = new ArrayCollection();
    }

    public function __clone() {
        if ($this->id) {
            $this->id = null;
        }
    }

    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->updateDate = new \DateTime();
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->updateDate = new \DateTime();
    }

    /**
     * Get id.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDeletedAt(): ?DateTimeInterface
    {
        return $this->deletedAt;
    }

    /**
     * @param DateTimeInterface|null $deletedAt
     */
    public function setDeletedAt(?DateTimeInterface $deletedAt): void
    {
        $this->deletedAt = $deletedAt;
    }

    /**
     * @return mixed
     */
    public function getUpdateDate()
    {
        return $this->updateDate;
    }

    /**
     * @param mixed $updateDate
     */
    public function setUpdateDate($updateDate): void
    {
        $this->updateDate = $updateDate;
    }

    /**
     * @return bool
     */
    public function isEntityAddress(): bool
    {
        return $this->entityAddress;
    }

    /**
     * @param bool $entityAddress
     */
    public function setEntityAddress(bool $entityAddress): void
    {
        $this->entityAddress = $entityAddress;
    }

    /**
     * @return string
     */
    public function getAddress(): ?string
    {
        if ($this->entityAddress && $this->getEntity()) {
            return $this->getEntity()->getAddress();
        }
        return $this->address;
    }

    /**
     * @param string|null $address
     */
    public function setAddress(?string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        if ($this->entityAddress && $this->getEntity()) {
            return $this->getEntity()->getCity();
        }
        return $this->city;
    }

    /**
     * @param string|null $city
     */
    public function setCity(?string $city): void
    {
        $this->city = $city;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile|null $imageFile
     */
    public function setImageFile(?File $imageFile = null): void
    {
        $this->imageFile = $imageFile;

        if (null !== $imageFile) {
            $this->updateDate =  new \DateTime();
        }
    }

    public function getImageFile(): ?File
    {
        return $this->imageFile;
    }

    public function setImageName(?string $imageName): void
    {
        $this->imageName = $imageName;
    }

    public function getImageName(): ?string
    {
        return $this->imageName;
    }

    public function setLogoSize(?int $eventImageSize): void
    {
        $this->eventImageSize = $eventImageSize;
    }

    public function getLogoSize(): ?int
    {
        return $this->eventImageSize;
    }

    /**
     * @return bool
     */
    public function isCycleInterval(): bool
    {
        return $this->cycleInterval;
    }

    /**
     * @param bool $cycleInterval
     */
    public function setCycleInterval(bool $cycleInterval): void
    {
        $this->cycleInterval = $cycleInterval;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     */
    public function setCategory(?Category $category): void
    {
        $this->category = $category;
    }

    /**
     * @return Entity|null
     */
    public function getEntity(): ?Entity
    {
        return $this->entity;
    }

    /**
     * @param Entity|null $entity
     */
    public function setEntity(?Entity $entity): void
    {
        $this->entity = $entity;
    }

    /**
     * @return UserInterface|null
     */
    public function getAddedBy(): ?UserInterface
    {
        return $this->addedBy;
    }

    /**
     * @param UserInterface|null $addedBy
     */
    public function setAddedBy(?UserInterface $addedBy): void
    {
        $this->addedBy = $addedBy;
    }

    /**
     * @return mixed
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param mixed $hash
     */
    public function setHash($hash): void
    {
        $this->hash = $hash;
    }

    public function generateHash(): string
    {
        return md5(random_bytes(10));
    }

    /**
     * @return string
     */
    public function getAgenda(): ?string
    {
        return $this->agenda;
    }

    /**
     * @param string $agenda
     */
    public function setAgenda(string $agenda): void
    {
        $this->agenda = $agenda;
    }

    /**
     * @return mixed
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @param mixed $startAt
     */
    public function setStartAt($startAt): void
    {
        $this->startAt = $startAt;
    }

    /**
     * @return mixed
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @param mixed $endAt
     */
    public function setEndAt($endAt): void
    {
        $this->endAt = $endAt;
    }

    /**
     * @return string
     */
    public function getExternalLink(): ?string
    {
        return $this->externalLink;
    }

    /**
     * @param string $externalLink
     */
    public function setExternalLink(string $externalLink): void
    {
        $this->externalLink = $externalLink;
    }

    /**
     * @return string
     */
    public function getTickets(): ?string
    {
        return $this->tickets;
    }

    /**
     * @param string $tickets
     */
    public function setTickets(string $tickets): void
    {
        $this->tickets = $tickets;
    }

    /**
     * @return bool
     */
    public function isFreeEntry(): ?bool
    {
        return $this->freeEntry;
    }

    /**
     * @param bool $freeEntry
     */
    public function setFreeEntry(bool $freeEntry): void
    {
        $this->freeEntry = $freeEntry;
    }

    /**
     * @return bool|null
     */
    public function isAccepted(): ?bool
    {
        return $this->accepted;
    }

    /**
     * @param bool|null $accepted
     */
    public function setAccepted(?bool $accepted): void
    {
        $this->accepted = $accepted;
    }

    /**
     * @return mixed
     */
    public function getEventImageSize()
    {
        return $this->eventImageSize;
    }

    /**
     * @param mixed $eventImageSize
     */
    public function setEventImageSize($eventImageSize): void
    {
        $this->eventImageSize = $eventImageSize;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     */
    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getInterval()
    {
        return $this->interval;
    }

    /**
     * @param mixed $interval
     */
    public function setInterval($interval): void
    {
        $this->interval = $interval;
    }

    /**
     * @return mixed
     */
    public function getCycle()
    {
        return $this->cycle;
    }

    /**
     * @param mixed $cycle
     */
    public function setCycle($cycle): void
    {
        $this->cycle = $cycle;
    }

    /**
     * @return mixed
     */
    public function getNewEntity()
    {
        return $this->newEntity;
    }

    /**
     * @param mixed $newEntity
     */
    public function setNewEntity($newEntity): void
    {
        $this->newEntity = $newEntity;
    }

    /**
     * @return mixed
     */
    public function getIsExistingEntity()
    {
        return $this->isExistingEntity;
    }

    /**
     * @param mixed $isExistingEntity
     */
    public function setIsExistingEntity($isExistingEntity): void
    {
        $this->isExistingEntity = $isExistingEntity;
    }

    public function getParticipants(): Collection
    {
        return $this->participants;
    }
    public function addParticipant(UserInterface $user): self
    {
        if (!$this->checkIfParticipant($user)) {
            $this->participants[] = $user;
            $user->addEvent($this);
        }
        return $this;
    }
    public function removeParticipant(UserInterface $user): self
    {
        if ($this->participants->contains($user)) {
            $this->participants->removeElement($user);
            $user->removeEvent($this);
        }
        return $this;
    }

    public function checkIfParticipant(UserInterface $user): bool
    {
        return $this->participants->contains($user);
    }
}

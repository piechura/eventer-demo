<?php
namespace App\Security;

use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserChecker implements UserCheckerInterface
{
    private $translation;
    private $em;

    public function __construct(TranslatorInterface $translation, EntityManagerInterface $em)
    {
        $this->translation = $translation;
        $this->em = $em;
    }

    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }

        if ($user->isDeleted()) {
            throw new CustomUserMessageAccountStatusException($this->translation->trans('user.account_not_exists'));
        }

        if (!$user->isActive()) {
            throw new CustomUserMessageAccountStatusException($this->translation->trans('user.account_not_active'));
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        if (!$user instanceof User) {
            return;
        }
        $user->setLastLoginDate(new \DateTime());
        $this->em->flush();
    }
}
<?php
namespace App\Service;

use App\Entity\Error;
use App\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;


class ExceptionLoggerHandler
{
    private $currentEnvironment;
    private $environment;
    private $tokenStorage;
    private $em;
    private $request;

    public function __construct(RequestStack $request, TokenStorageInterface $tokenStorage, EntityManagerInterface $em)
    {
        $this->currentEnvironment = $_ENV['APP_ENV'];
        $this->environment = ['dev', 'prod'];
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->request = $request;
    }

    public function handleException(\Throwable $exception): void
    {
        if ($this->isHandling($exception)) {
            $this->write($exception);
        }
    }

    private function isHandling(\Throwable $exception): bool
    {
        return $this->isEnvironmentValid();
    }

    private function isEnvironmentValid(): bool
    {
        return in_array($this->currentEnvironment, $this->environment);
    }

    protected function prepareRequestData(): ?string
    {
        $dataArray = [];
        $dataArray['queryString'] = $this->request->getCurrentRequest()->getQueryString();
        $dataArray['parameters'] = $this->request->getCurrentRequest()->request->all() ?? '';
        $dataArray['attributes'] = $this->request->getCurrentRequest()->attributes->all() ?? '';

        return json_encode($dataArray, JSON_PRETTY_PRINT);
    }

    protected function write(\Throwable $exception): void
    {
        if (!$this->em->isOpen()) {
            $this->em = $this->em->create(
                $this->em->getConnection(),
                $this->em->getConfiguration()
            );
        }

        $userId = '';
        if ($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();
            if ($user instanceof User) {
                $userId = $user->getId();
            } elseif(is_string($user)) {
                $userId = $user;
            }
        }

        try {
            $error = new Error();
            $error->setAction($exception->getMessage());
            $error->setRequest($this->prepareRequestData());
            $error->setTrace($exception->getTraceAsString());
            $error->setUserId($userId);
            $this->em->persist($error);
            $this->em->flush();
        } catch (\Exception $e) {
        }
    }
}
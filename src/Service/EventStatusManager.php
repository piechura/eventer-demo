<?php

namespace App\Service;

use App\Entity\Event;
use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;

class EventStatusManager
{
    protected $em;
    protected $eventRepository;

    public function __construct(EntityManagerInterface $em, EventRepository $eventRepository)
    {
        $this->em = $em;
        $this->eventRepository = $eventRepository;
    }

    public function findEvents(int $minutes)
    {
        return $this->eventRepository->findEventsByInterval($minutes);
    }

    public function changeEventsStatus(array $events): int
    {
        $changedItems = 0;
        $now = new \DateTime();
        foreach ($events as $event) {
            if ($event->getStartAt() <= $now && $event->getEndAt() >= $now) {
                $event->setStatus(Event::ONGOING);
                $changedItems++;
            } elseif ($event->getEndAt() <= $now) {
                $event->setStatus(Event::PAST);
                $changedItems++;
            }
        }
        $this->em->flush();
        return $changedItems;
    }
}

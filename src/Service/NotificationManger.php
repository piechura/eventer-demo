<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class NotificationManger
{
    protected $translator;
    protected $session;

    public function __construct(SessionInterface $session, TranslatorInterface $translator)
    {
        $this->session = $session;
        $this->translator = $translator;
    }

    public function setFlashNotification(string $action, string $status): void
    {
        if ($action == 'create') {
            $this->setCreateNotification($status);
        } elseif ($action == 'update') {
            $this->setUpdateNotification($status);
        } elseif ($action == 'remove') {
            $this->setRemoveNotification($status);
        }
    }

    public function setCustomFlashNotification(string $status, string $message)
    {
        $this->session->getFlashBag()->add($status, $message);
    }

    private function setCreateNotification(string $status): void
    {
        if ($status == 'success') {
            $this->session->getFlashBag()->add('success', $this->translator->trans('notification.success_created'));
        } elseif ($status == 'error') {
            $this->session->getFlashBag()->add('error', $this->translator->trans('notification.error_created'));
        }
    }

    private function setUpdateNotification(string $status): void
    {
        if ($status == 'success') {
            $this->session->getFlashBag()->add('success', $this->translator->trans('notification.success_updated'));
        } elseif ($status == 'error') {
            $this->session->getFlashBag()->add('error', $this->translator->trans('notification.error_updated'));
        }
    }

    private function setRemoveNotification(string $status): void
    {
        if ($status == 'success') {
            $this->session->getFlashBag()->add('success', $this->translator->trans('notification.success_removed'));
        } elseif ($status == 'error') {
            $this->session->getFlashBag()->add('error', $this->translator->trans('notification.error_removed'));
        }
    }
}

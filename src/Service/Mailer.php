<?php

namespace App\Service;

use App\Entity\Mail;
use App\Exception\MailerException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Validator\Constraints\Email;

class Mailer
{
    protected $em;
    protected $attachmentFiles;
    protected $validator;
    protected $params;

    public function __construct(
        EntityManagerInterface $em,
        AttachmentFiles $attachmentFiles,
        ValidatorInterface $validator,
        ParameterBagInterface $params
    ) {
        $this->em = $em;
        $this->attachmentFiles = $attachmentFiles;
        $this->validator = $validator;
        $this->params = $params;
    }

    public function createMail(array $mailerArray)
    {
        try {
            $mail = new Mail();
            $mail->setUser($mailerArray['user'] ?? null);
            $mail->setFromEmail($this->params->get('notification_from_email'));
            $mail->setFromName($this->params->get('notification_from_name'));
            $mail->setBody($mailerArray['body']);
            $mail->setAltBody(strip_tags($mailerArray['body']));
            $mail->setToEmail($mailerArray['toEmail']);
            $mail->setType($mailerArray['type']);
            $mail->setSubject($mailerArray['subject']);

            $filesArray = $this->attachmentFiles->saveFiles($mailerArray['attachments'] ?? []);
            $mail->setFiles($filesArray);

            $errors = $this->validator->validate($mail->getToEmail(), [new Email]);
            if (count($errors) == 0) {
                $this->em->persist($mail);
                $this->em->flush();

                if ($this->em->contains($mail)) {
                    return true;
                }
            }
            throw new MailerException(implode([','], $errors));
        } catch (\Exception $e) {
            throw new MailerException($e->getMessage() . 'Args:' . json_encode($mailerArray));
        }
    }
}
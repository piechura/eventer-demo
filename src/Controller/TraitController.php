<?php

namespace App\Controller;

use App\Service\NotificationManger;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use WhiteOctober\BreadcrumbsBundle\Model\Breadcrumbs;

class TraitController extends AbstractController
{
    private $breadcrumbs;
    private $notificationManger;
    private $translator;
    private $security;

    public function __construct(
        Breadcrumbs $breadcrumbs,
        NotificationManger $notificationManger,
        TranslatorInterface $translator,
        Security $security
    ) {
        $this->breadcrumbs = $breadcrumbs;
        $this->notificationManger = $notificationManger;
        $this->translator = $translator;
        $this->security = $security;
    }

    public function getBreadcrumbs(): Breadcrumbs
    {
        return $this->breadcrumbs;
    }

    public function getNotification(): NotificationManger
    {
        return $this->notificationManger;
    }

    public function getTrans(): TranslatorInterface
    {
        return $this->translator;
    }

    public function getSecurity(): Security
    {
        return $this->security;
    }
}

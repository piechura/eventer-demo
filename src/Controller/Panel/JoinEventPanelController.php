<?php

namespace App\Controller\Panel;

use App\Controller\TraitController;
use App\DataTables\Panel\JoinedEventPanelDataTable;
use App\Entity\Event;
use App\Form\Panel\DataTable\EventJoinedFilter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/panel/joined-events")
 * @IsGranted("ROLE_USER")
 */
class JoinEventPanelController extends TraitController
{
    /**
     * @Route("/list", methods="GET", name="panel_joined_event_list")
     */
    public function listAction(SessionInterface $session): Response
    {
        $tableId = JoinedEventPanelDataTable::ID;
        $sessionData = $session->get($tableId);
        $filterForm = $this->createForm(EventJoinedFilter::class, $sessionData);
        $this->setListBreadcrumb();
        return $this->render('panel/joinedEvents/list.html.twig',
            [
                'tableId' => $tableId,
                'dataUrl' => $this->generateUrl('panel_datatable', ['id' => $tableId]),
                'filterForm' => $filterForm->createView(),
            ]
        );
    }

    /**
     * @Route("/remove/{id}", methods="POST", name="panel_joined_event_remove")
     */
    public function remove(Event $event): Response
    {
        if ($event) {
            $em = $this->getDoctrine()->getManager();
            $event->removeParticipant($this->getUser());
            $em->flush();
            $this->getNotification()->setFlashNotification('remove', 'success');
        } else {
            $this->getNotification()->setFlashNotification('remove', 'error');
        }
        return $this->redirectToRoute('panel_joined_event_list');
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.joined_events'), $this->generateUrl('panel_joined_event_list'));
    }
}

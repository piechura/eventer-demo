<?php

namespace App\Controller\Panel;

use App\Controller\TraitController;
use App\Form\CalendarType;
use App\Repository\EventRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/panel/calendar")
 * @IsGranted("ROLE_USER")
 */
class CalendarPanelController extends TraitController
{
    /**
     * @Route("/", methods="GET", name="panel_calendar")
     */
    public function index(SessionInterface $session): Response
    {
        $sessionData = $session->get(CalendarType::class);
        $filterForm = $this->createForm(CalendarType::class, $sessionData, ['hideAddedBy' => true]);
        $this->setListBreadcrumb();

        return $this->render('panel/calendar/index.html.twig', [
            'filterForm' => $filterForm->createView(),
        ]);
    }

    /**
     * @Route("/events-from-dates/{start}/{end}", methods="POST", name="panel_calendar_events_from_dates")
     */
    public function ajaxCalendarEventsForDates(
        $start,
        $end,
        Request $request,
        SessionInterface $session,
        EventRepository $eventRepository
    ) {
        $startDate = new \DateTime($start);
        $endDate = new \DateTime($end);
        $filters = $request->get('filters');
        $filters['addedBy'] = $this->getSecurity()->getUser()->getId();
        $session->set(CalendarType::class, $filters);
        $events = $eventRepository->findEventsBetweenDates($startDate, $endDate, $filters);

        return $this->render('common/calendar/eventListContent.html.twig', [
            'events' => $events
        ]);
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.calendar'), $this->generateUrl('panel_calendar'));
    }
}

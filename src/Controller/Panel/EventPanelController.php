<?php

namespace App\Controller\Panel;

use App\Controller\TraitController;
use App\DataTables\Panel\EventPanelDataTable;
use App\Entity\Event;
use App\Form\DataTable\EventFilter;
use App\Form\EventPreviewType;
use App\Form\Panel\EventType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/panel/events")
 * @IsGranted("ROLE_USER")
 */
class EventPanelController extends TraitController
{
    /**
     * @Route("/event-details/{id}", methods="GET", name="panel_event_details_modal")
     */
    public function ajaxCalendarEventDetails(Event $event)
    {
        $form = $this->createForm(EventPreviewType::class, $event, ['disabled' => true]);
        return $this->render('common/calendar/modals/eventDetailsContent.html.twig', [
            'form' => $form->createView(),
            'event' => $event,
        ]);
    }

    /**
     * @Route("/list", methods="GET", name="panel_event_list")
     */
    public function listAction(SessionInterface $session): Response
    {
        $tableId = EventPanelDataTable::ID;
        $sessionData = $session->get($tableId);
        $filterForm = $this->createForm(EventFilter::class, $sessionData, ['hideAddedBy' => true]);
        $this->setListBreadcrumb();
        return $this->render('panel/events/list.html.twig',
            [
                'tableId' => $tableId,
                'dataUrl' => $this->generateUrl('panel_datatable', ['id' => $tableId]),
                'filterForm' => $filterForm->createView(),
            ]
        );
    }

    /**
     * @Route("/remove/{id}", methods="POST", name="panel_event_remove")
     */
    public function remove(Event $event): Response
    {
        if ($event) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
            $this->getNotification()->setFlashNotification('remove', 'success');
        } else {
            $this->getNotification()->setFlashNotification('remove', 'error');
        }
        return $this->redirectToRoute('admin_event_list');
    }

    /**
     * @Route("/new", methods="GET|POST", name="panel_event_new")
     */
    public function newAction(Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.new'));

        $event = new Event();
        $form = $this->getForm($event, ['type' => '']);
        if ($this->handleForm($form, $request, $event)) {
            return $this->redirectToRoute('panel_event_new_success');
        }
        return $this->render('panel/events/form.html.twig', [
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }
    /**
     * @Route("/new/success", methods="GET", name="panel_event_new_success")
     */
    public function successCreatedAction(Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.new'));

        return $this->render('panel/events/success.html.twig', [
        ]);
    }

    /**
     * @Route("/edit/{id}", methods="GET|POST", name="panel_event_edit")
     */
    public function editAction(Event $event, Request $request): Response
    {
        if ($event->isAccepted() && $this->getUser()->isUser()) {
            $this->getNotification()->setCustomFlashNotification(
                'error',
                $this->getTrans()->trans('notification.event_accepted_cant_be_edited')
            );
            return $this->redirectToRoute('panel_event_list');
        }

        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.edit'));

        $form = $this->getForm($event);
        if ($this->handleForm($form, $request, $event)) {
            return $this->redirectToRoute('panel_event_new_success');
        }

        return $this->render('panel/events/form.html.twig', [
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.events'), $this->generateUrl('panel_event_list'));
    }

    private function getForm(Event $event, array $options = []): FormInterface
    {
        return $this->createForm(EventType::class, $event, $options);
    }

    private function handleForm(FormInterface $form, Request $request, Event $event): bool
    {
        $action = $event->getId() ? 'update' : 'create';
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if (!$event->getId()) {
                    $em->persist($event);
                }
                $em->flush();
                return true;
            }
            $this->getNotification()->setFlashNotification($action, 'error');
        }
        return false;
    }
}

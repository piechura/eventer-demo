<?php

namespace App\Controller\Panel;

use App\Controller\TraitController;
use DataTables\DataTablesInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/panel/datatables-panel")
 * @IsGranted("ROLE_USER")
 */
class DataTablesPanelController extends TraitController
{
    /**
     * @Route("/{id}", name="panel_datatable")
     */
    public function dataTableAction(DataTablesInterface $dataTables, Request $request, string $id): JsonResponse
    {
        try {
            $results = $dataTables->handle($request, $id);
            return $this->json($results);
        }
        catch (HttpException $e) {
            return $this->json($e->getMessage(), $e->getStatusCode());
        }
    }
}

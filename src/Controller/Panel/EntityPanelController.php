<?php

namespace App\Controller\Panel;

use App\Controller\TraitController;
use App\DataTables\Panel\EntityPanelDataTable;
use App\Entity\Entity;
use App\Form\Panel\EntityType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/panel/entities")
 * @IsGranted("ROLE_USER")
 */
class EntityPanelController extends TraitController
{
    /**
     * @Route("/list", methods="GET", name="panel_entity_list")
     */
    public function listAction(): Response
    {
        $tableId = EntityPanelDataTable::ID;
        $this->setListBreadcrumb();

        return $this->render('panel/entities/list.html.twig',
            [
                'tableId' => $tableId,
                'dataUrl' => $this->generateUrl('panel_datatable', ['id' => $tableId]),
            ]
        );
    }

    /**
     * @Route("/remove/{id}", methods="POST", name="panel_entity_remove")
     */
    public function remove(Entity $entity): Response
    {
        if ($entity) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($entity);
            $em->flush();
            $this->getNotification()->setFlashNotification('remove', 'success');
        } else {
            $this->getNotification()->setFlashNotification('remove', 'error');
        }
        return $this->redirectToRoute('panel_entity_list');
    }

    /**
     * @Route("/new", methods="GET|POST", name="panel_entity_new")
     */
    public function newAction(Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.new'));

        $entity = new Entity();
        $form = $this->getForm($entity);
        if ($this->handleForm($form, $request, $entity)) {
            return $this->redirectToRoute('panel_entity_list');
        }

        return $this->render('common/entities/form.html.twig', [
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", methods="GET|POST", name="panel_entity_edit")
     */
    public function editAction(Entity $entity, Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.edit'));

        $form = $this->getForm($entity);
        if ($this->handleForm($form, $request, $entity)) {
            return $this->redirectToRoute('panel_entity_list');
        }

        return $this->render('common/entities/form.html.twig', [
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.entities'), $this->generateUrl('panel_entity_list'));
    }

    private function getForm(Entity $entity): FormInterface
    {
        return $this->createForm(EntityType::class, $entity);
    }

    private function handleForm(FormInterface $form, Request $request, Entity $entity): bool
    {
        $action = $entity->getId() ? 'update' : 'create';
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if (!$entity->getId()) {
                    $entity->addUser($this->getUser());
                    $em->persist($entity);
                }
                $em->flush();
                $this->getNotification()->setFlashNotification($action, 'success');
                return true;
            }
            $this->getNotification()->setFlashNotification($action, 'error');
        }
        return false;
    }
}

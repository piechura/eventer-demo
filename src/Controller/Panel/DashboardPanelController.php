<?php

namespace App\Controller\Panel;

use App\Controller\TraitController;
use App\Repository\CategoryRepository;
use App\Repository\EntityRepository;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/panel/dashboard")
 * @IsGranted("ROLE_USER")
 */
class DashboardPanelController extends TraitController
{
    /**
     * @Route("/", methods="GET", name="panel_dashboard")
     */
    public function index(EventRepository $eventRepository): Response
    {
        $this->setListBreadcrumb();
        $totalEvents = $eventRepository->getActiveEventCount(null, $this->getUser()->getId());
        $totalEventsLastMonth = $eventRepository->getActiveEventCount(new \DateTime('-1 month'), $this->getUser()->getId());

        $acceptedEvents = $eventRepository->findEventsBetweenDates(
            new \DateTime(),
            (new \DateTime('+1 month'))->setTime(23, 59, 59),
            ['addedBy' => $this->getUser()->getId(), 'accepted' => true]
        );

        $notAcceptedEvents = $eventRepository->findEventsBetweenDates(
            new \DateTime(),
            (new \DateTime('+1 month'))->setTime(23, 59, 59),
            ['addedBy' => $this->getUser()->getId(), 'accepted' => false]
        );

        return $this->render('panel/dashboard/index.html.twig', [
            'totalEvents' => $totalEvents,
            'totalEventsLastMonth' => $totalEventsLastMonth,
            'acceptedEvents' => $acceptedEvents,
            'notAcceptedEvents' => $notAcceptedEvents,
        ]);
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.dashboard'), $this->generateUrl('panel_dashboard'));
    }
}

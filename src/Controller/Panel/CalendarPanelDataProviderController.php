<?php

namespace App\Controller\Panel;

use App\Controller\CalendarDataProviderController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_USER")
 */
class CalendarPanelDataProviderController extends CalendarDataProviderController
{
    protected function getExtraParameters(): array
    {
        return ['addedBy' => $this->security->getUser()->getId()];
    }
}
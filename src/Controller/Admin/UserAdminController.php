<?php

namespace App\Controller\Admin;

use App\Controller\TraitController;
use App\DataTables\Admin\UsersAdminDataTable;
use App\Entity\User;
use App\Form\DataTable\UserFilter;
use App\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/users")
 * @IsGranted("ROLE_ADMIN")
 */
class UserAdminController extends TraitController
{
    /**
     * @Route("/list", methods="GET", name="admin_user_list")
     */
    public function list(SessionInterface $session): Response
    {
        $tableId = UsersAdminDataTable::ID;
        $sessionData = $session->get($tableId);
        $filterForm = $this->createForm(UserFilter::class, $sessionData);

        $this->setListBreadcrumb();
        return $this->render('admin/users/list.html.twig', [
            'tableId' => $tableId,
            'filterForm' => $filterForm->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", methods="GET|POST", name="admin_user_edit")
     */
    public function edit(Request $request, User $user): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans("action.edit"));

        $form = $this->getForm($user);
        if ($this->handleForm($form, $request, $user)) {
            return $this->redirectToRoute('admin_user_list');
        }

        return $this->render('admin/users/edit.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/remove/{id}", methods="POST", name="admin_user_remove")
     */
    public function remove(User $user): Response
    {
        if ($user) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($user);
            $em->flush();
            $this->getNotification()->setFlashNotification('remove', 'success');
        } else {
            $this->getNotification()->setFlashNotification('remove', 'error');
        }
        return $this->redirectToRoute('admin_user_list');
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()->addItem(
            $this->getTrans()->trans('label.users'), $this->get("router")->generate("admin_user_list")
        );
    }

    private function getForm(User $user, array $options = []): FormInterface
    {
        return $this->createForm(UserType::class, $user, $options);
    }

    private function handleForm(FormInterface $form, Request $request, User $user): bool
    {
        $action = $user->getId() ? 'update' : 'create';
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if (!$user->getId()) {
                    $em->persist($user);
                }
                $em->flush();
                $this->getNotification()->setFlashNotification($action, 'success');
                return true;
            }
            $this->getNotification()->setFlashNotification($action, 'error');
        }
        return false;
    }
}

<?php

namespace App\Controller\Admin;

use App\Controller\TraitController;
use App\Repository\CategoryRepository;
use App\Repository\EntityRepository;
use App\Repository\EventRepository;
use App\Repository\UserRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/dashboard")
 * @IsGranted("ROLE_ADMIN")
 */
class DashboardAdminController extends TraitController
{
    /**
     * @Route("/", methods="GET", name="admin_dashboard")
     */
    public function index(
        EventRepository $eventRepository,
        CategoryRepository $categoryRepository,
        EntityRepository $entityRepository,
        UserRepository $userRepository
    ): Response {
        $this->setListBreadcrumb();

        $totalUser = $userRepository->getActiveUserCount();
        $totalEvents = $eventRepository->getActiveEventCount();
        $totalEntity = $entityRepository->getActiveEntityCount();

        $totalUserLastMonth = $userRepository->getActiveUserCount(new \DateTime('-1 month'));
        $totalEventsLastMonth = $eventRepository->getActiveEventCount(new \DateTime('-1 month'));
        $totalEntityLastMonth = $entityRepository->getActiveEntityCount(new \DateTime('-1 month'));

        $categories = $categoryRepository->getCategoriesWithEventsNumb();
        $entities = $entityRepository->getEntitiesWithEventsNumb();

        $events = $eventRepository->findEventsBetweenDates(
            new \DateTime(),
            (new \DateTime('+2 days'))->setTime(23, 59, 59)
        );

        $notAcceptedEvents = $eventRepository->findEventsBetweenDates(
            new \DateTime(),
            (new \DateTime('+1 month'))->setTime(23, 59, 59),
            ['accepted' => false]
        );

        return $this->render('admin/dashboard/index.html.twig', [
            'events' => $events,
            'categories' => $categories,
            'entities' => $entities,
            'totalUser' => $totalUser,
            'totalEvents' => $totalEvents,
            'totalEntity' => $totalEntity,
            'notAcceptedEvents' => count($notAcceptedEvents),
            'totalUserLastMonth' => $totalUserLastMonth,
            'totalEventsLastMonth' => $totalEventsLastMonth,
            'totalEntityLastMonth' => $totalEntityLastMonth,
        ]);
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.dashboard'), $this->generateUrl('admin_dashboard'));
    }
}

<?php

namespace App\Controller\Admin;

use App\Controller\CalendarDataProviderController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_ADMIN")
 */
class CalendarAdminDataProviderController extends CalendarDataProviderController
{
    protected function getExtraParameters(): array
    {
        return [];
    }
}
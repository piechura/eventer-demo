<?php

namespace App\Controller\Admin;

use App\Controller\TraitController;
use DataTables\DataTablesInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/admin/datatables-admin")
 * @IsGranted("ROLE_ADMIN")
 */
class DataTablesAdminController extends TraitController
{
    /**
     * @Route("/{id}", name="admin_datatable")
     */
    public function dataTableAction(DataTablesInterface $dataTables, Request $request, string $id): JsonResponse
    {
        try {
            $results = $dataTables->handle($request, $id);
            return $this->json($results);
        }
        catch (HttpException $e) {
            return $this->json($e->getMessage(), $e->getStatusCode());
        }
    }
}

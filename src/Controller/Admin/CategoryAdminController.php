<?php

namespace App\Controller\Admin;

use App\Controller\TraitController;
use App\DataTables\Admin\CategoryAdminDataTable;
use App\Entity\Category;
use App\Form\CategoryType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/categories")
 * @IsGranted("ROLE_ADMIN")
 */
class CategoryAdminController extends TraitController
{
    /**
     * @Route("/list", methods="GET", name="admin_category_list")
     */
    public function listAction(): Response
    {
        $tableId = CategoryAdminDataTable::ID;
        $this->setListBreadcrumb();

        return $this->render('admin/categories/list.html.twig',
            [
                'tableId' => $tableId,
            ]
        );
    }

    /**
     * @Route("/remove/{id}", methods="POST", name="admin_category_remove")
     */
    public function remove(Category $category): Response
    {
        if ($category) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush();
            $this->getNotification()->setFlashNotification('remove', 'success');
        } else {
            $this->getNotification()->setFlashNotification('remove', 'error');
        }
        return $this->redirectToRoute('admin_category_list');
    }

    /**
     * @Route("/new", methods="GET|POST", name="admin_category_new")
     */
    public function newAction(Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.new'));

        $category = new Category();
        $form = $this->getForm($category);
        if ($this->handleForm($form, $request, $category)) {
            return $this->redirectToRoute('admin_category_list');
        }

        return $this->render('admin/categories/form.html.twig', [
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", methods="GET|POST", name="admin_category_edit")
     */
    public function editAction(Category $category, Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.edit'));

        $form = $this->getForm($category);
        if ($this->handleForm($form, $request, $category)) {
            return $this->redirectToRoute('admin_category_list');
        }

        return $this->render('admin/categories/form.html.twig', [
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.categories'), $this->generateUrl('admin_category_list'));
    }

    private function getForm(Category $category): FormInterface
    {
        return $this->createForm(CategoryType::class, $category);
    }

    private function handleForm(FormInterface $form, Request $request, Category $category): bool
    {
        $action = $category->getId() ? 'update' : 'create';
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if (!$category->getId()) {
                    $em->persist($category);
                }
                $em->flush();
                $this->getNotification()->setFlashNotification($action, 'success');
                return true;
            }
            $this->getNotification()->setFlashNotification($action, 'error');
        }
        return false;
    }
}

<?php

namespace App\Controller\Admin;

use App\Controller\TraitController;
use App\DataTables\Admin\MailAdminDataTable;
use App\Entity\Mail;
use App\Entity\User;
use App\Form\DataTable\MessageFilter;
use App\Service\AttachmentFiles;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/admin/messages")
 * @IsGranted("ROLE_ADMIN")
 */
class MessagesAdminController extends TraitController
{
    /**
     * @Route("/list", methods="GET", name="admin_messages_list")
     * @Route("/list/{user}", methods="GET", name="admin_user_messages_list")
     */
    public function listAction(SessionInterface $session, User $user = null): Response
    {
        $tableId = MailAdminDataTable::ID;
        $this->setListBreadcrumb($user);
        $sessionData = $session->get($tableId);
        if ($user) {
            $sessionData['userId'] = $user->getId();
        }

        $filterForm = $this->createForm(MessageFilter::class, $sessionData);
        return $this->render('admin/messages/list.html.twig',
            [
                'filterForm' => $filterForm->createView(),
                'tableId' => $tableId,
                'userId' => $user ? $user->getId() : null
            ]
        );
    }

    /**
     * @Route("/remove/{id}", methods="POST", name="admin_message_remove")
     * @Route("/remove/{id}/{user}", methods="POST", name="admin_user_message_remove")
     */
    public function removeMessage(Mail $mail, User $user = null): Response
    {
        if ($mail) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($mail);
            $em->flush();
            $this->getNotification()->setFlashNotification('remove', 'success');
        } else {
            $this->getNotification()->setFlashNotification('remove', 'error');
        }
        return $this->redirect($this->getListUrl($user));
    }

    /**
     * @Route("/preview/{id}", methods="GET", name="admin_message_preview")
     * @Route("/preview/{id}/{user}", methods="GET", name="admin_user_message_preview")
     */
    public function previewAction(Mail $mail, AttachmentFiles $attachmentFiles, User $user = null): Response
    {
        $this->setListBreadcrumb($user);
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.message_show'));

        $filesMap = $attachmentFiles->getLocationFiles($mail->getFiles());

        return $this->render('admin/messages/preview.html.twig', [
            'mail' => $mail,
            'filesMap' => $filesMap,
            'backUrl' => $this->getListUrl($user),
        ]);
    }

    /**
     * @Route("/html/{mail}", methods="GET", name="message_html")
     */
    public function htmlAction(Mail $mail): Response
    {
        return new Response($mail->getBody());
    }

    private function setListBreadcrumb(?User $user): void
    {
        $label = $user
            ? $this->getTrans()->trans('label.messages_of_user') . ': ' . $user->getEmail()
            : $this->getTrans()->trans('label.messages');

        $this->getBreadcrumbs()->addItem($label, $this->getListUrl($user));
    }

    private function getListUrl(?User $user): string
    {
        if ($user) {
            return $this->generateUrl('admin_user_messages_list', ['user' => $user->getId()]);
        } else {
            return $this->generateUrl('admin_messages_list');
        }
    }
}
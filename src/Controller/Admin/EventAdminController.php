<?php

namespace App\Controller\Admin;

use App\Controller\TraitController;
use App\DataTables\Admin\EventAdminDataTable;
use App\Entity\Event;
use App\Form\DataTable\EventFilter;
use App\Form\EventPreviewType;
use App\Form\EventType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin/events")
 * @IsGranted("ROLE_ADMIN")
 */
class EventAdminController extends TraitController
{
    /**
     * @Route("/event-details/{id}", methods="GET", name="admin_event_details_modal")
     */
    public function ajaxCalendarEventDetails(Event $event)
    {
        $form = $this->createForm(EventPreviewType::class, $event, ['disabled' => true]);
        return $this->render('common/calendar/modals/eventDetailsContent.html.twig', [
            'form' => $form->createView(),
            'event' => $event,
        ]);
    }

    /**
     * @Route("/not-accepted-list", methods="GET", name="admin_event_list_not_accepted")
     */
    public function listNotAcceptedAction(SessionInterface $session): Response
    {
        $tableId = EventAdminDataTable::ID;
        $sessionData = $session->get($tableId);
        $sessionData['accepted'] = false;
        $session->set($tableId, $sessionData);
        return $this->redirectToRoute('admin_event_list');
    }

    /**
     * @Route("/list", methods="GET", name="admin_event_list")
     */
    public function listAction(SessionInterface $session): Response
    {
        $tableId = EventAdminDataTable::ID;
        $sessionData = $session->get($tableId);
        $filterForm = $this->createForm(EventFilter::class, $sessionData);
        $this->setListBreadcrumb();
        return $this->render('admin/events/list.html.twig',
            [
                'tableId' => $tableId,
                'dataUrl' => $this->generateUrl('admin_datatable', ['id' => $tableId]),
                'filterForm' => $filterForm->createView(),
            ]
        );
    }

    /**
     * @Route("/remove/{id}", methods="POST", name="admin_event_remove")
     */
    public function remove(Event $event): Response
    {
        if ($event) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($event);
            $em->flush();
            $this->getNotification()->setFlashNotification('remove', 'success');
        } else {
            $this->getNotification()->setFlashNotification('remove', 'error');
        }
        return $this->redirectToRoute('admin_event_list');
    }

    /**
     * @Route("/accept/{id}", methods="POST", name="admin_event_accept")
     */
    public function accept(Event $event, Request $request): Response
    {
        $referer = $request->headers->get('referer');
        if ($event) {
            if ($event->isAccepted()) {
                $this->getNotification()->setFlashNotification('update', 'error');
                return $this->redirect($referer);
            }
            $em = $this->getDoctrine()->getManager();
            $event->setAccepted(true);
            $em->flush();
            $this->getNotification()->setFlashNotification('update', 'success');
        } else {
            $this->getNotification()->setFlashNotification('update', 'error');
        }
        return $this->redirect($referer);
    }

    /**
     * @Route("/new", methods="GET|POST", name="admin_event_new")
     */
    public function newAction(Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.new'));

        $event = new Event();
        $form = $this->getForm($event, ['type' => 'adding']);
        if ($this->handleForm($form, $request, $event)) {
            return $this->redirectToRoute('admin_event_list');
        }
        return $this->render('admin/events/form.html.twig', [
            'action' => 'new',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", methods="GET|POST", name="admin_event_edit")
     */
    public function editAction(Event $event, Request $request): Response
    {
        $this->setListBreadcrumb();
        $this->getBreadcrumbs()->addItem($this->getTrans()->trans('action.edit'));

        $form = $this->getForm($event);
        if ($this->handleForm($form, $request, $event)) {
            return $this->redirectToRoute('admin_event_list');
        }

        return $this->render('admin/events/form.html.twig', [
            'action' => 'edit',
            'form' => $form->createView(),
        ]);
    }

    private function setListBreadcrumb(): void
    {
        $this->getBreadcrumbs()
            ->addItem($this->getTrans()->trans('label.events'), $this->generateUrl('admin_event_list'));
    }

    private function getForm(Event $event, array $options = []): FormInterface
    {
        return $this->createForm(EventType::class, $event, $options);
    }

    private function handleForm(FormInterface $form, Request $request, Event $event): bool
    {
        $action = $event->getId() ? 'update' : 'create';
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                if (!$event->getId()) {
                    $em->persist($event);
                }
                $em->flush();
                $this->getNotification()->setFlashNotification($action, 'success');
                return true;
            }
            $this->getNotification()->setFlashNotification($action, 'error');
        }
        return false;
    }
}

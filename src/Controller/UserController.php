<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\NewUserType;
use App\Form\Type\ChangePasswordType;
use App\Form\Type\NewPasswordType;
use App\Form\UserType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Controller used to manage current user.
 * @Route("/user")
 */
class UserController extends TraitController
{
    /**
     * @Route("/activate-token/{token}", methods="GET|POST", name="user_activate_token")
     */
    public function activateTokenPassword(string $token): Response
    {
        $repository = $this->getDoctrine()->getRepository(User::class);
        $user = $repository->getUserByActivationToken($token);

        if ($user == null ) {
            throw new BadRequestHttpException(
                $this->getTrans()->trans('request_status.bad_request')
            );
        }

        $user->setIsActive(true);
        $user->setActiveToken(null);
        $this->getDoctrine()->getManager()->flush();

        return $this->render('common/user/activationTokenSuccess.html.twig');
    }

    /**
     * @Route("/register", methods="GET|POST", name="register_user")
     */
    public function registerUserAction(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        $form = $this->createForm(NewUserType::class, $user);

        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $em = $this->getDoctrine()->getManager();
                $user->setPassword($passwordEncoder->encodePassword($user, $user->getPassword()));
                $user->setRoles([User::ROLE_USER]);
                $em->persist($user);
                $em->flush();
                return $this->redirectToRoute('register_user_success');
            }
        }
        return $this->render('common/user/register.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register/success", methods="GET|POST", name="register_user_success")
     */
    public function registerUserSuccessAction(Request $request): Response
    {
        return $this->render('common/user/registerSuccess.html.twig');
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @Route("/edit", methods="GET|POST", name="user_edit")
     */
    public function edit(Request $request): Response
    {
        $this->getBreadcrumbs()->addItem(
            $this->getTrans()->trans('label.profile'), $this->get("router")->generate("user_edit")
        );

        $user = $this->getUser();
        $form = $this->createForm(UserType::class, $user, [
            'disableEmail' => false,
            'hideIsActive' => true,
            'hideRoles' => true,
            'hideEntities' => true
            ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success',  $this->getTrans()->trans('user.updated_successfully'));
            return $this->redirectToRoute('user_edit');
        }

        return $this->render('user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     * @Route("/change-password", methods="GET|POST", name="user_change_password")
     */
    public function changePassword(Request $request, UserPasswordEncoderInterface $encoder): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user->getPassword() && $user->isSocialAuthenticated()) {
            $form = $this->createForm(NewPasswordType::class);
        } else {
            $form = $this->createForm(ChangePasswordType::class);
        }

        $this->getBreadcrumbs()->addItem(
            $this->getTrans()->trans('label.change_password'), $this->get("router")->generate("user_change_password")
        );

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($encoder->encodePassword($user, $form->get('newPassword')->getData()));
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash('success',  $this->getTrans()->trans('user.change_password_successfully'));

            return $this->redirectToRoute('user_change_password');
        }

        return $this->render('user/changePassword.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}

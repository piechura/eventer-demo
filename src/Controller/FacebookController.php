<?php

namespace App\Controller;

use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class FacebookController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     * @param ClientRegistry $clientRegistry
     *
     * @Route("/connect/facebook", name="connect_facebook_start")
     *
     * @return RedirectResponse
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('facebook')
            ->redirect(
                ['public_profile', 'email'], []
            );
    }

    /**
     * @param Security $security
     * @Route("/connect/facebook/check", name="connect_facebook_check")
     * @return RedirectResponse
     */
    public function connectCheckAction(Security $security)
    {
        if ($security->isGranted('ROLE_USER')) {
            return $this->redirectToRoute('front_index');
        }
        if ($security->isGranted('ROLE_ADMIN')) {
            return $this->redirectToRoute('admin_dashboard');
        }
        throw new \Exception('bad role');
    }
}
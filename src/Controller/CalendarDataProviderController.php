<?php

namespace App\Controller;

use CalendarBundle\CalendarEvents;
use CalendarBundle\Controller\CalendarController;
use CalendarBundle\Event\CalendarEvent;
use CalendarBundle\Serializer\Serializer;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class CalendarDataProviderController extends CalendarController
{
    protected $security;
    protected $start;
    protected $end;
    protected $filters = [];

    public function __construct(EventDispatcherInterface $eventDispatcher, Serializer $serializer, Security $security)
    {
        parent::__construct($eventDispatcher, $serializer);
        $this->security = $security;
    }

    public function loadAction(Request $request): Response
    {
        $this->getParameters($request);
        $event = $this->dispatchWithBC(
            new CalendarEvent($this->start, $this->end, $this->filters),
            CalendarEvents::SET_DATA
        );
        $content = $this->serializer->serialize($event->getEvents());
        return $this->getResponse($content);
    }

    protected function getExtraParameters(): array
    {
        return [];
    }

    protected function getParameters(Request $request): void
    {
        $this->start = new \DateTime($request->get('start'));
        $this->end = new \DateTime($request->get('end'));
        $filters = $request->get('filters', '{}');
        $filters = \is_array($filters) ? $filters : json_decode($filters, true);
        $this->filters = array_merge($filters, $this->getExtraParameters());
    }

    protected function getResponse(?string $content): Response
    {
        $response = new Response();
        $response->headers->set('Content-Type', 'application/json');
        $response->setContent($content);
        $response->setStatusCode(empty($content) ? Response::HTTP_NO_CONTENT : Response::HTTP_OK);
        return $response;
    }

}
<?php

namespace App\Controller\Front;

use App\Controller\TraitController;
use App\Entity\Event;
use App\Entity\User;
use App\Repository\EventRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/app/events")
 */
class JoinedEventsFrontController extends TraitController
{
    /**
     * @Route("/", methods="GET", name="front_events_joined")
     * @IsGranted("IS_AUTHENTICATED_FULLY")
     */
    public function index(EventRepository $eventRepository): Response
    {
        $incomingJoinedEvents = $eventRepository->findJoinedEventsByUser($this->getUser()->getId(), Event::INCOMING);
        $pastJoinedEvents = $eventRepository->findJoinedEventsByUser($this->getUser()->getId(), Event::PAST);

        return $this->render('front/events/joinedEvents.html.twig', [
            'incomingJoinedEvents' => $incomingJoinedEvents,
            'pastJoinedEvents' => $pastJoinedEvents
        ]);
    }

    /**
     * @Route("/event-join/{id}", methods="POST", name="front_event_join")
     */
    public function ajaxCalendarEventDetails(Event $event) {

        if (!$event instanceof Event || !$event->isAccepted() || $event->isDeleted()) {
            throw $this->createNotFoundException($this->getTrans()->trans('exceptions.event_not_found'));
        }

        if (!$this->getUser() instanceof User) {
            return $this->render('front/calendar/modalEventJoin.html.twig', [
                'status' => 'login'
            ]);
        }

        if ($event->checkIfParticipant($this->getUser())) {
            return $this->render('front/calendar/modalEventJoin.html.twig', [
                'status' => 'already_joined'
            ]);
        }

        $em = $this->getDoctrine()->getManager();
        $event->addParticipant($this->getUser());
        $em->flush();

        return $this->render('front/calendar/modalEventJoin.html.twig', [
            'status' => 'successful_joined'
        ]);
    }


    /**
     * @Route("/unjoin-event/{id}", methods="POST", name="front_event_unjoin")
     */
    public function ajaxUnjoinEvent(Event $event)
    {
        if (!$event instanceof Event || !$event->isAccepted() || $event->isDeleted()) {
            throw $this->createNotFoundException($this->getTrans()->trans('exceptions.event_not_found'));
        }

        if (!$this->getUser() instanceof User) {
            throw $this->createNotFoundException($this->getTrans()->trans('exceptions.user_not_found'));
        }
        if ($event->checkIfParticipant($this->getUser())) {
            $em = $this->getDoctrine()->getManager();
            $event->removeParticipant($this->getUser());
            $em->flush();
        }
        return $this->render('front/calendar/modalEventJoin.html.twig', [
            'status' => 'successful_unjoined'
        ]);
    }
}

<?php

namespace App\Controller\Front;

use App\Controller\CalendarDataProviderController;

class CalendarFrontDataProviderController extends CalendarDataProviderController
{
    protected function getExtraParameters(): array
    {
        return ['accepted' => true];
    }
}
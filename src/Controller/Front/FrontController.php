<?php

namespace App\Controller\Front;

use App\Controller\TraitController;
use App\Entity\Event;
use App\Form\Front\CalendarFrontType;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * @Route("/app")
 */
class FrontController extends TraitController
{
    /**
     * @Route("/", methods="GET", name="front_index")
     */
    public function index(SessionInterface $session, RouterInterface $router): Response
    {
        $sessionData = $session->get(CalendarFrontType::class);
        $filterForm = $this->createForm(CalendarFrontType::class, $sessionData);
        return $this->render('front/calendar/index.html.twig', [
            'filterForm' => $filterForm->createView()
        ]);
    }

    /**
     * @Route("/events-from-dates/{start}/{end}", methods="POST", name="front_events_list_from_dates")
     */
    public function ajaxEventsListForDates(
        $start,
        $end,
        Request $request,
        SessionInterface $session,
        EventRepository $eventRepository
    ) {
        $startDate = new \DateTime($start);
        $endDate = new \DateTime($end);
        $filters = $request->get('filters');
        $filters['accepted'] = true;
        $session->set(CalendarFrontType::class, $filters);
        $events = $eventRepository->findEventsBetweenDates($startDate, $endDate, $filters);

        return $this->render('front/calendar/eventList.html.twig', [
            'events' => $events
        ]);
    }

    /**
     * @Route("/event-detail/{id}", methods="GET", name="front_event_details")
     */
    public function ajaxEventDetails(Event $event) {
        if (!$event instanceof Event || !$event->isAccepted() || $event->isDeleted()) {
            throw $this->createNotFoundException($this->getTrans()->trans('exceptions.event_not_found'));
        }

        return $this->render('front/calendar/modalEventDetails.html.twig', [
            'event' => $event
        ]);
    }
}

<?php

namespace App\Controller\Front;

use App\Controller\TraitController;
use App\Entity\Event;
use App\Entity\User;
use App\Form\CalendarType;
use App\Form\Front\CalendarFrontType;
use App\Repository\EventRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/app/user")
 */
class UserFrontController extends TraitController
{
    /**
     * @Route("/", methods="GET", name="front_user")
     */
    public function index(): Response
    {
        $filterForm = $this->createForm(CalendarFrontType::class);
        return $this->render('front/calendar/index.html.twig', [
            'filterForm' => $filterForm->createView()
        ]);
    }
}

<?php

namespace App\Command;

use App\Entity\Event;
use App\Service\EventStatusManager;
use App\Utils\Validator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EventStatusCommand extends Command
{
    protected static $defaultName = 'events:status-update';

    private $io;
    private $validator;
    private $eventStatusManager;

    public function __construct(Validator $validator, EventStatusManager $eventStatusManager)
    {
        parent::__construct();
        $this->validator = $validator;
        $this->eventStatusManager = $eventStatusManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Update event\'s status')
            ->addArgument('minutes', InputArgument::REQUIRED, 'Interval in minutes')
            ->setHelp('');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (null !== $input->getArgument('minutes')) {
            return;
        }
        $this->io->title('Update event\'s status depends on interval.');
        $this->io->text([
            'Define interval in minutes as argument.',
            'All events between now and time from now minus interval minutes will be processed.',
            '',
        ]);

        $minutes = $this->io->ask('Minutes', null, [$this->validator, 'validateMinutes']);
        $input->setArgument('minutes', $minutes);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $minutes = $this->validator->validateMinutes($input->getArgument('minutes'));
        /** @var Event[] $events */
        $events = $this->eventStatusManager->findEvents($minutes);
        if (empty($events)) {
            throw new RuntimeException(sprintf('Events with interval "%s" minutes not found.', $minutes));
        }
        $changedItems = $this->eventStatusManager->changeEventsStatus($events);
        $this->io->success(sprintf('Successfully changed items counts %d', $changedItems));

        return Command::SUCCESS;
    }
}

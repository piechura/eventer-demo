<?php

namespace App\Command;

use App\Entity\Mail;
use App\Service\AttachmentFiles;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Send Emails from the DB.
 *
 */
class SendEmailCommand extends Command
{
    /** @var \Swift_Mailer */
    private $mailer;

    /** @var EntityManagerInterface */
    private $em;

    /** @var AttachmentFiles */
    private $attachmentFiles;

    /** @var KernelInterface */
    private $kernel;

    /**
     * SendEmailCommand constructor.
     * @param \Swift_Mailer $mailer
     * @param EntityManagerInterface $em
     * @param AttachmentFiles $attachmentFiles
     * @param KernelInterface $kernel
     */
    public function __construct(\Swift_Mailer $mailer, EntityManagerInterface $em, AttachmentFiles $attachmentFiles, KernelInterface $kernel)
    {
        parent::__construct();
        $this->mailer = $mailer;
        $this->em = $em;
        $this->attachmentFiles = $attachmentFiles;
        $this->kernel = $kernel;
    }

    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('app:mailer:send')
            ->setDescription('Sends emails from the DB')
            ->addOption(
                'limit',
                null,
                InputOption::VALUE_REQUIRED,
                'The maximum number of messages to send.',
                20
            )
            ->setHelp(<<<EOF
The <info>app:mailer:send</info> command sends all emails from the spool.
<info>php bin/console app:mailer:send --limit=20</info>
EOF
            )
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $emailsToSend = $this->getEmailsToSend((int)$input->getOption('limit'));
        $sentNumb = 0;
        $failedNumb = 0;

        /** @var Mail $item */
        foreach ($emailsToSend as $item) {
            $message = (new \Swift_Message($item->getSubject()))
                ->setFrom($_ENV['FROM_MAIL'], $_ENV['FROM_NAME'])
                ->setTo($item->getToEmail())
                ->setBody($item->getBody(),'text/html')
            ;

            if (!empty($item->getFiles()) && is_array($item->getFiles())) {
                $files = $this->attachmentFiles->getLocationFiles($item->getFiles());
                foreach ($files as $fileName => $location) {
                    $message->attach(\Swift_Attachment::fromPath($this->kernel->getProjectDir().'/public'.$location));
                }
            }

            if ($this->mailer->send($message)) {
                $sentNumb++;
                $item->setIsSent(true);
                $item->setSentAt(new \DateTime());
            } else {
                $failedNumb++;
                $item->setIsFailed(true);
            }
        }

        $this->em->flush();
        $output->writeln('Finish');
        $output->writeln('Sent: ' . $sentNumb);
        $output->writeln('Failed: ' . $failedNumb);
        return Command::SUCCESS;

    }


    private function getEmailsToSend(int $limit)
    {
        return $this->em->getRepository(Mail::class)->findNext($limit);
    }
}

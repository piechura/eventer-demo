<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Entity;
use App\Entity\Event;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    private $passwordEncoder;
    private $slugger;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder, SluggerInterface $slugger)
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadCategories($manager);
        $this->loadUsers($manager);
        $this->loadEntities($manager);
        $this->loadEvents($manager);
    }

    private function loadEvents(ObjectManager $manager): void
    {
        $categoryRepository = $manager->getRepository(Category::class);
        $entityRepository = $manager->getRepository(Entity::class);
        $userRepository = $manager->getRepository(User::class);
        foreach ($this->getEventData() as [$category, $entity, $name, $description, $entityAddress, $tickets]) {
            $categoryItem = $categoryRepository->findOneBy(['name' => $category]);
            $eventItem = $entityRepository->findOneBy(['name' => $entity]);
            $userItem = $userRepository->findOneBy(['email' => 'tester_user@symfony.com']);

            $eventDay = rand(1, 30);
            $eventStartHour = rand(8, 19);
            $eventEndHour = $eventStartHour+(rand(1, 5));

            $startDate = date('Y-m') . '-' . $eventDay . ' ' . $eventStartHour . ':00';
            $endDate = date('Y-m') . '-' . $eventDay . ' ' . $eventEndHour . ':00';

            $event = new Event();
            $event->setCategory($categoryItem);
            $event->setEntity($eventItem);
            $event->setAddedBy($userItem);
            $event->setStatus(0);
            $event->setName($name);
            $event->setDescription($description);
            $event->setEntityAddress($entityAddress);
            $event->setTickets($tickets);
            $event->setStartAt(new \DateTime($startDate));
            $event->setEndAt(new \DateTime($endDate));
            $event->setFreeEntry(false);
            $event->setCycleInterval(false);

            $manager->persist($event);
        }
        $manager->flush();

    }

    private function loadUsers(ObjectManager $manager): void
    {
        foreach ($this->getUserData() as [$fullname, $username, $password, $email, $isActive, $roles]) {
            $user = new User();
            $user->setFullName($fullname);
            $user->setUsername($username);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $password));
            $user->setEmail($email);
            $user->setIsActive($isActive);
            $user->setRoles($roles);
            $manager->persist($user);
            $this->addReference($username, $user);
        }
    }

    private function loadCategories(ObjectManager $manager): void
    {
        foreach ($this->getCategoriesData() as [$name, $description, $color]) {
            $category = new Category();
            $category->setName($name);
            $category->setDescription($description);
            $category->setColor($color);
            $manager->persist($category);
        }
        $manager->flush();
    }

    private function loadEntities(ObjectManager $manager): void
    {
        foreach ($this->getEntityData() as [$name, $description, $address, $city, $contactEmail]) {
            $entity = new Entity();
            $entity->setName($name);
            $entity->setDescription($description);
            $entity->setAddress($address);
            $entity->setCity($city);
            $entity->setContactEmail($contactEmail);
            $manager->persist($entity);
        }
        $manager->flush();
    }

    private function getEntityData(): array
    {
        return [
            // $entityData = [$name, $description, $address, $city, $contactEmail];
            ['Entity test 1', 'Entity test desc”.', 'Witolda Budryka 4', 'Kraków', 'entity_test_1@gmail.com'],
            ['Klub Studio', 'Klub studencki w Krakowie administrowany przez Fundację Studentów i Absolwentów AGH „Academica”.', 'Witolda Budryka 4', 'Kraków', 'entity_test_1@gmail.com'],
            ['Babilon. Dom studencki', 'Dom Studencki AGH', 'Radziszewskiego 17', 'Kraków', 'entity_test_2@gmail.com'],
            ['Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Wydział Wiertnictwa, Nafty i Gazu AGH – jeden z 16 wydziałów Akademii Górniczo-Hutniczej im. Stanisława Staszica w Krakowie.',  'Al. Mickiewicza 30, Paw. A-4, pok. 123',  'Kraków', 'entity_test_3@gmail.com'],
        ];
    }

    private function getCategoriesData()
    {
        return [
            // $categoryData = [$name, $description, $color];
            ['Category test 1', 'Category desc', '#022'],
            ['Koncert', 'Koncert scenyjny', '#000'],
            ['Spotkanie informacyjne', 'Spotkanie o różnym charakterze', '#F0F'],
            ['Warsztaty studenckie', 'Warsztaty studenckie', '#008080'],
            ['Webinaria', 'Webinaria internetowe', '#B91A5A'],
            ['Koła studenckie', 'Koła studenckie', '#ffd000'],
        ];
    }

    private function getUserData(): array
    {
        return [
            // $userData = [$fullname, $username, $password, $email, $isActive, $roles];
            ['Tester Admin', 'tester_admin', 'kitten', 'tester_admin@symfony.com', true, ['ROLE_ADMIN']],
            ['Tester user', 'tester_user', 'kitten', 'tester_user@symfony.com', true, ['ROLE_USER']],
        ];
    }

    private function getEventData(): array
    {
        return [
            // $eventData =[$category, $entity, $name, $description, $entityAddress, $tickets];
            ['Koncert', 'Klub Studio', 'Koncert O.S.T.R - Premiera płyty', 'Koncert O.S.T.R - Premiera płyty', true, 'Bilety pod stroną ...'],
            ['Koncert', 'Klub Studio', 'Koncert DonDugralesko', 'Koncert DonDugralesko', true, 'Bilety pod stroną ...'],
            ['Koncert', 'Klub Studio', 'Problem - National Geographic', 'Koncert Problem', true, 'Bilety pod stroną ...'],
            ['Koncert', 'Klub Studio', 'Bitamina', 'Koncert Bitamina', true, 'Bilety pod stroną ...'],
            ['Koncert', 'Klub Studio', 'Myslovic', 'Koncert Myslovic', true, 'Bilety pod stroną ...'],
            ['Koncert', 'Klub Studio', 'Monika Brodka - Męskie granie', 'Monika Brodka - Męskie granie', true, 'Bilety pod stroną ...'],

            ['Spotkanie informacyjne', 'Babilon. Dom studencki', 'Spotkanie dla pierwszaków', 'Monika Brodka - Męskie granie', true, 'Bilety pod stroną ...'],
            ['Spotkanie informacyjne', 'Babilon. Dom studencki', 'Wybór rady mieszkańców', 'Wybór rady mieszkańców', true, 'Bilety pod stroną ...'],

            ['Warsztaty studenckie', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Warsztaty plastyczne', 'Warsztaty plastyczne', true, 'Bilety pod stroną ...'],
            ['Warsztaty studenckie', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Warsztaty wiertnicze', 'Warsztaty wiertnicze', true, 'Bilety pod stroną ...'],
            ['Webinaria', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Prezentacja "Gaz łupkowy"', 'Prezentacja', true, 'Bilety pod stroną ...'],
            ['Webinaria', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Szkolenie MS Office', 'Szkolenie', true, 'Bilety pod stroną ...'],
            ['Webinaria', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Szkolenie DrillPack', 'Szkolenie', true, 'Bilety pod stroną ...'],
            ['Webinaria', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Szkolenie Landmark Eclipse', 'Szkolenie', true, 'Bilety pod stroną ...'],
            ['Koła studenckie', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Koło "Geowiert"', 'Szkolenie', true, 'Bilety pod stroną ...'],
            ['Koła studenckie', 'Wydział Wiertnictwa, Nafty i Gazu - AGH', 'Koło "Nafta"', 'Szkolenie', true, 'Bilety pod stroną ...'],
        ];
    }
}

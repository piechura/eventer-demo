<?php

namespace App\Exception;


class DataTableMissingConfigException extends \RuntimeException
{
    protected $message = 'Missing params config for datatable';
}

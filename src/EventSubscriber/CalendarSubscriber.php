<?php

namespace App\EventSubscriber;

use App\Form\CalendarType;
use App\Repository\EventRepository;
use CalendarBundle\CalendarEvents;
use App\Entity\Event as EventEntity;
use CalendarBundle\Entity\Event;
use CalendarBundle\Event\CalendarEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class CalendarSubscriber implements EventSubscriberInterface
{
    protected $eventRepository;
    protected $session;

    public function __construct(EventRepository $eventRepository, SessionInterface $session)
    {
        $this->eventRepository = $eventRepository;
        $this->session = $session;
    }

    public static function getSubscribedEvents()
    {
        return [
            CalendarEvents::SET_DATA => 'onCalendarSetData',
        ];
    }

    public function onCalendarSetData(CalendarEvent $calendar)
    {
        $start = $calendar->getStart();
        $end = $calendar->getEnd();
        $filters = $calendar->getFilters();

        $this->session->set(CalendarType::class, $filters);
        $events = $this->eventRepository->findEventsBetweenDates($start, $end, $filters);

        /** @var EventEntity $event */
        foreach ($events as $event) {
            $calendar->addEvent(new Event(
                $event->getName() . ' - ' . $event->getEntity()->getName(),
                $event->getStartAt(),
                $event->getEndAt(),
                [
                    'id' => $event->getId(),
                    'isAccepted' => $event->isAccepted(),
                    'categoryColor' => $event->getCategory()->getColor(),
                ]
            ));
        }
    }
}
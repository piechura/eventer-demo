<?php
namespace App\EventSubscriber;

use App\Service\ExceptionLoggerHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    protected $exceptionLoggerHandler;

    public function __construct(ExceptionLoggerHandler $exceptionLoggerHandler)
    {
        $this->exceptionLoggerHandler = $exceptionLoggerHandler;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => [
                ['logException', 0],
            ],
        ];
    }

    public function logException(ExceptionEvent $event)
    {
        $this->exceptionLoggerHandler->handleException($event->getThrowable());
    }
}
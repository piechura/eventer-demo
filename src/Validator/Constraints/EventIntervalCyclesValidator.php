<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventIntervalCyclesValidator extends ConstraintValidator
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param \App\Entity\Event $event
     * @param Constraint $constraint
     */
    public function validate($event, Constraint $constraint)
    {

        if (!empty($event->getCycle()) || !empty($event->getInterval())) {
            if (empty($event->getCycle())) {
                $this->context->buildViolation($this->translator->trans($constraint->messageEmptyCycle))
                    ->atPath('cycle')
                    ->addViolation();
            }

            if (empty($event->getInterval())) {
                $this->context->buildViolation($this->translator->trans($constraint->messageEmptyInterval))
                    ->atPath('interval')
                    ->addViolation();
            }

            if (!empty($event->getCycle()) && !empty($event->getInterval())) {
                $event->setCycleInterval(true);
            }
        } else {
            $event->setCycleInterval(false);
        }
    }
}
<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventTimeValidator extends ConstraintValidator
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param \App\Entity\Event $event
     * @param Constraint $constraint
     */
    public function validate($event, Constraint $constraint)
    {
        if ($event->getStartAt() >= $event->getEndAt()) {
            $this->context->buildViolation($this->translator->trans($constraint->message))
                ->atPath('endAt')
                ->addViolation();
        }
    }
}
<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EventIntervalCycles extends Constraint
{
    public $messageEmptyCycle = 'label.cycle_must_be_selected_with_interval';
    public $messageEmptyInterval = 'label.interval_must_be_selected_with_cycle';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
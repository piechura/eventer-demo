<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EventAddress extends Constraint
{
    public $message = 'label.value_is_required';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
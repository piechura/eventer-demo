<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventAddressValidator extends ConstraintValidator
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @param \App\Entity\Event $event
     * @param Constraint $constraint
     */
    public function validate($event, Constraint $constraint)
    {
        if ($event->isEntityAddress() == false && (empty($event->getCity()) || empty($event->getAddress()))) {
            if (empty($event->getAddress())) {
                $this->context->buildViolation($this->translator->trans($constraint->message))
                    ->atPath('address')
                    ->addViolation();
            }
            if (empty($event->getCity())) {
                $this->context->buildViolation($this->translator->trans($constraint->message))
                    ->atPath('city')
                    ->addViolation();
            }
        }

        /** clear address if it's entity */
        if ($event->isEntityAddress() == true && (!empty($event->getCity()) || !empty($event->getAddress()))) {
            $event->setCity(null);
            $event->setAddress(null);
        }
    }
}
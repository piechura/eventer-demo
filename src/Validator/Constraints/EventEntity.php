<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EventEntity extends Constraint
{
    public $message = 'label.entity_is_required';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

}
<?php
namespace App\Validator\Constraints;

use Symfony\Component\Security\Core\Security;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventEntityValidator extends ConstraintValidator
{
    private $translator;
    private $security;

    public function __construct(TranslatorInterface $translator, Security $security)
    {
        $this->translator = $translator;
        $this->security = $security;
    }

    /**
     * @param \App\Entity\Event $event
     * @param Constraint $constraint
     */
    public function validate($event, Constraint $constraint)
    {
        if ($event->getIsExistingEntity() && !$event->getEntity()) {
            $this->context->buildViolation($this->translator->trans($constraint->message))
                ->atPath('entity')
                ->addViolation();
        }

        if ($event->getIsExistingEntity() === 0) {
            $newEntity = $event->getNewEntity();
            $newEntity->addUser($this->security->getUser());
            $event->setEntity($newEntity);
        }
    }
}
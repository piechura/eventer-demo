<?php
namespace App\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class EventTime extends Constraint
{
    public $message = 'label.end_at_must_be_greater_than_start_at';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
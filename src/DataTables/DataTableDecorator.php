<?php

namespace App\DataTables;

trait DataTableDecorator
{
    protected function getLink(array $buttonData): string
    {
        if ($buttonData['type'] == 'delete-with-confirmation') {
            return $this->createDeleteLinkWithConfirmation($buttonData['url'], $buttonData['label']);
        } elseif($buttonData['type'] == 'unjoin-event-action') {
            return $this->createUnjoinLinkWithConfirmation($buttonData['url'], $buttonData['label']);
        } elseif($buttonData['type'] == 'preview-event-action') {
            return $this->createEventPreviewLink($buttonData['urlPreview'], $buttonData['urlEdit'], $buttonData['label'], $buttonData['type']);
        } elseif($buttonData['type'] == 'accept-event-action') {
            return $this->createEventAcceptLink($buttonData['urlAccept'], $buttonData['label'], $buttonData['type']);
        } else {
            return $this->createLink($buttonData['url'], $buttonData['label'], $buttonData['target']);
        }
    }

    protected function getDate(?\DateTime $date, $format = 'Y-m-d H:i:s'): string
    {
        return ($date && $date instanceof \DateTime) ? $date->format($format) : '-';
    }

    protected function createActionsButtonWithLinks(array $buttonsArray, string $buttonLabel): string
    {
        if (empty($buttonsArray)) {
            return '';
        }

        $buttons = '';
        foreach ($buttonsArray as $button) {
            $buttons .= '<li>' . $this->getLink($button) . '</li>';
        }

        return '<div class="btn-group text-right">
                    <button type="button" class="btn btn-primary br2 btn-xs fs12 dropdown-toggle" data-toggle="dropdown" aria-expanded="false">'. $buttonLabel . '
                        <span class="caret ml5"></span>
                    </button>
                    <ul class="dropdown-menu" role="menu">' . $buttons . '</ul>
                 </div>';
    }

    protected function createLink(string $url, string $label, string $target = '_self'): string
    {
        if ($url) {
            return '<a  href="' . $url . '" target="'.$target.'"> ' . $label . '</a>';
        }
        return '';
    }

    protected function createEventAcceptLink(string $urlAccept, string $label, string $type): string
    {
        if (!empty($urlAccept) && !empty($label) && !empty($type)) {
            return '<a class="' . $type . '" data-event-accept="' . $urlAccept . '">' . $label . '</a>';
        }
        return '';
    }

    protected function createEventPreviewLink(string $urlPreview, string $urlEdit, string $label, string $type): string
    {
        if (!empty($urlPreview) && !empty($urlEdit) && !empty($label) && !empty($type)) {
            return '<a class="' . $type . '" data-event-preview="' . $urlPreview . '" data-event-edit="' . $urlEdit . '">' . $label . '</a>';
        }
        return '';
    }

    protected function createUnjoinLinkWithConfirmation(string $url, string $label)
    {
        if (!empty($url) && !empty($label)) {
            return '<a class="unjoin-action-confirmation" data-action="' . $url . '">' . $label . '</a>';
        }
        return '';
    }

    protected function createDeleteLinkWithConfirmation(string $url, string $label): string
    {
        if (!empty($url) && !empty($label)) {
            return '<a class="delete-action-confirmation" data-action="' . $url . '">' . $label . '</a>';
        }
        return '';
    }

    protected function createButton(string $url, string $label, string $buttonType): string
    {
        if ($url) {
            return '<a  href="' . $url . '">
                <button type="button" class="btn ladda-button mt5 btn-'.$buttonType.'"  data-style="contract">
                    <span class="ladda-label">'.$label.'</span>
                    <span class="ladda-spinner"></span>
                </button>
            </a>';
        }
        return '';

    }

    protected function createButtonWithConfirmation(string $url, string $label, string $buttonType): string
    {
        return '<a>
                    <button type="button" class="btn ladda-button mt5 btn-' . $buttonType . ' delete-action-confirmation" data-style="zoom-in"  data-action="' . $url . '">
                        <span class="ladda-label">' . $label . '</span>
                        <span class="ladda-spinner"></span>
                    </button>
                </a>';
    }

    protected function createActivityLabel(bool $activity, string $activeLabel, string $inactiveLabel): string
    {
        if ($activity) {
            return '<span class="label label-success">' . $activeLabel . '</span>';
        } else {
            return '<span class="label label-danger">' . $inactiveLabel . '</span>';
        }
    }

    protected function getColorLabel(string $color, $label = ''): string
    {
        return '<span class="label" style="background-color: ' . $color . '">' . ($label === '' ? $color : $label) . '</span>';
    }
}


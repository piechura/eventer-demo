<?php

namespace App\DataTables;

use App\Exception\DataTableMissingConfigException;
use App\Utils\ArrayManager;
use DataTables\AbstractDataTableHandler;
use DataTables\DataTableQuery;
use DataTables\DataTableResults;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ObjectRepository;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class AppDataTableHandler extends AbstractDataTableHandler
{
    use DataTableDecorator;

    protected $tableId = null;
    protected $masterClass = null;
    protected $masterAlias = null;
    protected $useSession = false;

    protected $doctrine;
    protected $router;
    protected $translation;
    protected $session;
    protected $arrayManager;
    protected $security;

    public function __construct(
        SessionInterface $session,
        EntityManagerInterface $em,
        UrlGeneratorInterface $router,
        TranslatorInterface $translation,
        ArrayManager $arrayManager,
        Security $security
    ) {
        $this->doctrine = $em;
        $this->router = $router;
        $this->translation = $translation;
        $this->session = $session;
        $this->arrayManager = $arrayManager;
        $this->security = $security;
    }

    public function handle(DataTableQuery $request): DataTableResults
    {
        $this->validateConfig();
        if ($this->isUseSession()) {
            $this->setFilterToSession($request, $this->getTableId());
        }
        $query = $this->initQuery();
        $this->setSearchQuery($query, $request);
        $this->setOrderQuery($query, $request);
        return $this->getResults($query, $request);
    }

    public function setFilterToSession(DataTableQuery $dataTableQuery, $tableId): void
    {
        if ($dataTableQuery->customData) {
            $this->session->set($tableId, $this->arrayManager->clearDataFromEmptyStrings($dataTableQuery->customData));
        }
    }

    abstract protected function buildResultsRow($class): array;
    abstract protected function setSearchQuery(QueryBuilder $query, DataTableQuery $request): QueryBuilder;
    abstract protected function getOrderColumns(): array;

    protected function validateConfig()
    {
        if (empty($this->getTableId())
            || empty($this->getMasterClass())
            || empty($this->getMasterAlias())
            || !is_string($this->getTableId())
            || !is_string($this->getMasterClass())
            || !is_string($this->getMasterAlias())
        ) {
            throw new DataTableMissingConfigException();
        }
    }

    protected function getOrderColumn($name = '', $alias = null)
    {
        return $alias ?? $this->getMasterAlias() . '.' . $name;
    }

    protected function getRepository(): ObjectRepository
    {
        return $this->doctrine->getRepository($this->getMasterClass());
    }

    protected function initQuery(): QueryBuilder
    {
        return $this->getRepository()
            ->createQueryBuilder($this->getMasterAlias())
            ->where($this->getMasterAlias() . '.deletedAt is null');
    }

    protected function setOrderQuery(QueryBuilder $query, DataTableQuery $request): QueryBuilder
    {
        foreach ($request->order as $order) {
            if (array_key_exists($order->column, $this->getOrderColumns())) {
                $query->addOrderBy($this->getOrderColumns()[$order->column], $order->dir);
            }
        }
        return $query;
    }

    protected function getTotalRecordsCount()
    {
        return $this->getRepository()
            ->createQueryBuilder($this->getMasterAlias())
            ->select('COUNT(' . $this->getMasterAlias() . '.id)')
            ->where($this->getMasterAlias() . '.deletedAt is null')
            ->getQuery()
            ->getSingleScalarResult();
    }

    protected function getResults(QueryBuilder $query, DataTableQuery $request): DataTableResults
    {
        $results = new DataTableResults();
        $results->recordsTotal = $this->getTotalRecordsCount();
        $entities = $query->getQuery()->getResult();
        $results->recordsFiltered = count($entities);

        for ($i = 0; $i < $request->length || $request->length == -1; $i++) {
            $index = $i + $request->start;
            if ($index >= $results->recordsFiltered) {
                break;
            }
            $entity = $entities[$index];
            $results->data[] = $this->buildResultsRow($entity);
        }
        return $results;
    }

    protected function getMasterClass(): ?string
    {
        return $this->masterClass;
    }


    protected function setMasterClass(string $masterClass): void
    {
        $this->masterClass = $masterClass;
    }

    protected function getMasterAlias(): ?string
    {
        return $this->masterAlias;
    }

    protected function setMasterAlias(string $masterAlias): void
    {
        $this->masterAlias = $masterAlias;
    }


    protected function isUseSession(): bool
    {
        return $this->useSession;
    }


    protected function setUseSession(bool $useSession): void
    {
        $this->useSession = $useSession;
    }

    protected function getTableId(): ?string
    {
        return $this->tableId;
    }

    protected function setTableId(string $tableId): void
    {
        $this->tableId = $tableId;
    }
}

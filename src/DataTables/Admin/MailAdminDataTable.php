<?php
namespace App\DataTables\Admin;

use App\DataTables\AppDataTableHandler;
use App\Entity\Mail;
use DataTables\DataTableQuery;
use Doctrine\ORM\QueryBuilder;


class MailAdminDataTable extends AppDataTableHandler
{
    private $userId;
    const ID = 'admin_emails_datatable';

    protected $tableId = self::ID;
    protected $masterClass = Mail::class;
    protected $masterAlias = 'm';
    protected $useSession = true;

    protected function getOrderColumns(): array
    {
        return [
            $this->getOrderColumn('id'),
            $this->getOrderColumn('toEmail'),
            $this->getOrderColumn('subject'),
            $this->getOrderColumn('createdAt'),
            $this->getOrderColumn('sentAt'),
            $this->getOrderColumn('isSent'),
        ];
    }

    /**
     * @param Mail $entity
     * @return array
     */
    protected function buildResultsRow($entity): array
    {
        return [
            $entity->getId(),
            $entity->getToEmail(),
            $entity->getSubject(),
            $entity->getType(),
            $this->getDate($entity->getCreatedAt()),
            $this->getDate($entity->getSentAt()),
            $this->createActivityLabel($entity->getIsSent(), $this->translation->trans('status.sent'), $this->translation->trans('status.notSent')),
            $this->createActionsButtonWithLinks(
                $this->getActionsDataArray($entity),
                $this->translation->trans('label.actions')
            )
        ];
    }

    protected function setSearchQuery(QueryBuilder $query, DataTableQuery $request): QueryBuilder
    {
        if ($request->search->value != "" && $request->search->value) {
            $query->andWhere('(LOWER(m.toEmail) LIKE :search OR ' .
                ' LOWER(m.subject) LIKE :search )'
            );
            $query->setParameter(
                'search',
                strtolower("%{$request->search->value}%")
            );
        }

        foreach ($request->customData as $customDatum => $datum) {
            $value = trim($datum);
            if ($customDatum == 'userId' && $value !== '') {
                $query->andWhere('m.user = :userId');
                $query->setParameter('userId', (int)$value);
            }
            if ($customDatum == 'toEmail' && $value !== '') {
                $query->andWhere('m.toEmail = :toEmail');
                $query->setParameter('toEmail', "{$value}");
            }
            if ($customDatum == 'fullName' && $value !== '') {
                $query->andWhere('LOWER(u.fullName) LIKE :fullName');
                $query->setParameter('fullName', "{$value}%");
            }
            if ($customDatum == 'isSent' && $value !== '') {
                $query->andWhere('m.isSent = :isSent');
                $query->setParameter('isSent', (bool)$value);
            }
            if ($customDatum == 'type' && $value !== '') {
                $query->andWhere('LOWER(m.type) LIKE :type');
                $query->setParameter('type', "{$value}%");
            }
            if ($customDatum == 'subject' && $value !== '') {
                $query->andWhere('LOWER(m.subject) LIKE :subject');
                $query->setParameter('subject', "{$value}%");
            }
        }

        return $query;
    }

    private function getActionsDataArray(Mail $entity): array
    {
        return [
            [
                'type' => 'basic',
                'url' => $this->getShowUrl($entity->getId()),
                'label' => $this->translation->trans('action.preview'),
                'target' => '_self'
            ],
            [
                'type' => 'delete-with-confirmation',
                'url' => $this->getDeleteUrl($entity->getId()),
                'label' => $this->translation->trans('action.delete'),
            ],
        ];
    }


    private function getShowUrl(int $id): string
    {
        if ($this->userId) {
            return $this->router->generate(
                'admin_user_message_preview',
                [
                    'id' => $id,
                    'user' => $this->userId,
                ]
            );
        }
        return $this->router->generate(
            'admin_message_preview',
            [
                'id' => $id
            ]
        );
    }

    private function getDeleteUrl(int $id): string
    {
        return $this->router->generate(
            'admin_message_remove',
            [
                'id' => $id
            ]
        );
    }
}

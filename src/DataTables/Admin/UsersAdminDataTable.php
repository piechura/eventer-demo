<?php
namespace App\DataTables\Admin;

use App\DataTables\AppDataTableHandler;
use App\Entity\User;
use DataTables\DataTableQuery;
use Doctrine\ORM\QueryBuilder;

class UsersAdminDataTable extends AppDataTableHandler
{
    const ID = 'admin_users';

    protected $tableId = self::ID;
    protected $masterClass = User::class;
    protected $masterAlias = 'u';
    protected $useSession = true;

    protected function getOrderColumns(): array
    {
        return [
            $this->getOrderColumn('id'),
            $this->getOrderColumn('fullName'),
            $this->getOrderColumn('email'),
            $this->getOrderColumn('roles'),
            $this->getOrderColumn('isActive'),
            $this->getOrderColumn('registerDate'),
            $this->getOrderColumn('updateDate'),
            $this->getOrderColumn('lastLoginDate'),
        ];
    }

    /**
     * @param User $entity
     * @return array
     */
    protected function buildResultsRow($entity): array
    {
        return [
            $entity->getId(),
            $entity->getFullName(),
            $entity->getEmail(),
            implode('<br>', $entity->getRoles()),
            $this->createActivityLabel($entity->isActive(), $this->translation->trans('status.active'), $this->translation->trans('status.inactive')),
            $this->getDate($entity->getRegisterDate()),
            $this->getDate($entity->getUpdateDate()),
            $this->getDate($entity->getLastLoginDate()),
            $this->createActionsButtonWithLinks(
                $this->getActionsDataArray($entity),
                $this->translation->trans('label.actions')
            )
        ];
    }

    protected function setSearchQuery(QueryBuilder $query, DataTableQuery $request): QueryBuilder
    {
        if ($request->search->value != '' && $request->search->value) {
            $query->where('(LOWER(u.fullName) LIKE :search OR' .
                ' LOWER(u.email) LIKE :search)');
            $query->setParameter(
                'search',
                strtolower("%{$request->search->value}%")
            );
        }

        foreach ($request->customData as $customDatum => $datum) {
            $value = trim($datum);
            if ($customDatum == 'name' && $value !== '') {
                $query->andWhere('LOWER(u.fullName) LIKE :name');
                $query->setParameter('name', strtolower("%{$value}%"));
            }
            if ($customDatum == 'email' && $value !== '') {
                $query->andWhere('LOWER(u.email) LIKE :email');
                $query->setParameter('email', strtolower("%{$value}%"));
            }
            if ($customDatum == 'roles' && $value !== '') {
                $query->andWhere('LOWER(u.roles) LIKE :roles');
                $query->setParameter('roles', strtolower("%{$value}%"));
            }
            if ($customDatum == 'activity' && $value !== '') {
                $query->andWhere('u.isActive = :activity');
                $query->setParameter('activity', (int)$value);
            }
        }
        return $query;
    }

    private function getActionsDataArray(User $entity): array
    {
        return [
            [
                'type' => 'basic',
                'url' => $this->getEditUrl($entity->getId()),
                'label' => $this->translation->trans('action.edit'),
                'target' => '_self'
            ],
            [
                'type' => 'basic',
                'url' => $this->getMessagesUrl($entity->getId()),
                'label' => $this->translation->trans('action.messages'),
                'target' => '_self'
            ],
            [
                'type' => 'delete-with-confirmation',
                'url' => $this->getDeleteUrl($entity->getId()),
                'label' => $this->translation->trans('action.delete'),
            ],
        ];
    }

    private function getEditUrl(int $id): string
    {
        return $this->router->generate(
            'admin_user_edit',
            [
                'id' => $id
            ]
        );
    }

    private function getMessagesUrl(int $id): string
    {
        return $this->router->generate(
            'admin_user_messages_list',
            [
                'user' => $id
            ]
        );
    }

    private function getDeleteUrl(int $id): string
    {
        return $this->router->generate(
            'admin_user_remove',
            [
                'id' => $id
            ]
        );
    }
}
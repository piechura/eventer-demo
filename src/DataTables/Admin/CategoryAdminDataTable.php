<?php
namespace App\DataTables\Admin;

use App\DataTables\AppDataTableHandler;
use App\Entity\Category;
use DataTables\DataTableQuery;
use Doctrine\ORM\QueryBuilder;

class CategoryAdminDataTable extends AppDataTableHandler
{
    const ID = 'admin_category_datatable';

    protected $tableId = self::ID;
    protected $masterClass = Category::class;
    protected $masterAlias = 'c';
    protected $useSession = false;

    protected function getOrderColumns(): array
    {
        return [
            $this->getOrderColumn('id'),
            $this->getOrderColumn('name'),
            $this->getOrderColumn('color'),
            $this->getOrderColumn('createdAt'),
            $this->getOrderColumn('updateDate'),
        ];
    }

    /**
     * @param Category $entity
     * @return array
     */
    protected function buildResultsRow($entity): array
    {
        return [
            $entity->getId(),
            $entity->getName(),
            $this->getColorLabel($entity->getColor()),
            $this->getDate($entity->getCreatedAt()),
            $this->getDate($entity->getUpdateDate()),
            $this->createActionsButtonWithLinks(
                $this->getActionsDataArray($entity),
                $this->translation->trans('label.actions')
            )
        ];
    }

    protected function setSearchQuery(QueryBuilder $query, DataTableQuery $request): QueryBuilder
    {
        if ($request->search->value != '' && $request->search->value) {
            $query->where('(LOWER(c.name) LIKE :search OR' .
                ' LOWER(c.description) LIKE :search)');
            $query->setParameter(
                'search',
                strtolower("%{$request->search->value}%")
            );
        }
        return $query;
    }

    private function getActionsDataArray(Category $entity): array
    {
        return [
            [
                'type' => 'basic',
                'url' => $this->getEditUrl($entity->getId()),
                'label' => $this->translation->trans('action.edit'),
                'target' => '_self'
            ],
            [
                'type' => 'delete-with-confirmation',
                'url' => $this->getDeleteUrl($entity->getId()),
                'label' => $this->translation->trans('action.delete'),
            ],
        ];
    }

    private function getEditUrl(int $id): string
    {
        return $this->router->generate(
            'admin_category_edit',
            [
                'id' => $id
            ]
        );
    }

    private function getDeleteUrl(int $id): string
    {
        return $this->router->generate(
            'admin_category_remove',
            [
                'id' => $id
            ]
        );
    }
}
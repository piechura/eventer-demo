<?php
namespace App\DataTables\Admin;

use App\DataTables\AppDataTableHandler;
use App\Entity\Event;
use App\Entity\User;
use DataTables\DataTableQuery;
use Doctrine\ORM\QueryBuilder;

class EventAdminDataTable extends AppDataTableHandler
{
    const ID = 'admin_events_datatable';

    protected $tableId = self::ID;
    protected $masterClass = Event::class;
    protected $masterAlias = 'e';
    protected $useSession = true;


    protected function getOrderColumns(): array
    {
        return [
            $this->getOrderColumn('id'),
            $this->getOrderColumn('name'),
            $this->getOrderColumn('category'),
            $this->getOrderColumn('entity'),
            $this->getOrderColumn('status'),
            $this->getOrderColumn('addedBy'),
            $this->getOrderColumn('accepted'),
            $this->getOrderColumn('startAt'),
            $this->getOrderColumn('endAt'),
            $this->getOrderColumn('createdAt'),
            $this->getOrderColumn('updateDate'),
        ];
    }

    /**
     * @param Event $entity
     * @return array
     */
    protected function buildResultsRow($entity): array
    {
        return [
            $entity->getId(),
            $entity->getName() . $this->getFreeEventLabel($entity->isFreeEntry()) . $this->getCycleEventLabel($entity->isCycleInterval()),
            $this->getColorLabel($entity->getCategory()->getColor(), $entity->getCategory()->getName()),
            $this->createLink(
                $this->getEditEntityUrl($entity->getEntity()->getId()),
                $entity->getEntity()->getName(),
                '_blank'
            ),
            $this->createStatusLabel($entity->getStatus()),
            $entity->getAddedBy()->getFullName(),
            $this->getAcceptanceLabel($entity->isAccepted()),
            $this->getDate($entity->getStartAt(), 'Y-m-d H:i'),
            $this->getDate($entity->getEndAt(), 'Y-m-d H:i'),
            $this->getDate($entity->getCreatedAt()),
            $this->getDate($entity->getUpdateDate()),
            $this->createActionsButtonWithLinks(
                $this->getActionsDataArray($entity),
                $this->translation->trans('label.actions')
            )
        ];
    }

    protected function setSearchQuery(QueryBuilder $query, DataTableQuery $request): QueryBuilder
    {
        if ($request->search->value != '' && $request->search->value) {
            $query->where('(LOWER(e.name) LIKE :search )');

            $query->setParameter(
                'search',
                strtolower("%{$request->search->value}%")
            );
        }

        foreach ($request->customData as $customDatum => $datum) {
            $value = trim($datum);
            if ($customDatum == 'name' && $value !== '') {
                $query->andWhere('LOWER(e.name) LIKE :name');
                $query->setParameter('name', strtolower("%{$value}%"));
            }
            if ($customDatum == 'category' && $value !== '') {
                $query->andWhere('e.category = :category');
                $query->setParameter('category', (int)$value);
            }
            if ($customDatum == 'addedBy' && $value !== '') {
                $query->andWhere('e.addedBy = :addedBy');
                $query->setParameter('addedBy', (int)$value);
            }
            if ($customDatum == 'status' && $value !== '') {
                $query->andWhere('e.status = :status');
                $query->setParameter('status', (int)$value);
            }
            if ($customDatum == 'accepted' && $value !== '') {
                $query->andWhere('e.accepted = :accepted');
                $query->setParameter('accepted', (bool)$value);
            }
        }
        return $query;
    }

    protected function getActionsDataArray(Event $entity): array
    {
        $actionsDataArray = [
            [
                'type' => 'preview-event-action',
                'urlPreview' => $this->getPreviewUrl($entity->getId()),
                'urlEdit' => $this->getEditUrl($entity->getId()),
                'label' => $this->translation->trans('action.preview'),
            ],
            [
                'type' => 'delete-with-confirmation',
                'url' => $this->getDeleteUrl($entity->getId()),
                'label' => $this->translation->trans('action.delete'),
            ],
        ];

        if ($entity->isAccepted() == false ||
            ($entity->isAccepted() && !$this->security->isGranted(User::ROLE_USER))) {
            $actionsDataArray[] = [
                'type' => 'basic',
                'url' => $this->getEditUrl($entity->getId()),
                'label' => $this->translation->trans('action.edit'),
                'target' => '_self'
            ];
        }

        if (!$entity->isAccepted() == true && !$this->security->isGranted(User::ROLE_USER)) {
            $actionsDataArray[] = [
                'type' => 'accept-event-action',
                'urlAccept' => $this->getAcceptUrl($entity->getId()),
                'label' => $this->translation->trans('action.accept'),
            ];
        }

        return $actionsDataArray;
    }

    protected function getAcceptanceLabel(bool $isAccepted): string
    {
        if ($isAccepted) {
            return '<span class="label label-success">' . $this->translation->trans('label.accepted') . '</span>';
        }
        return '<span class="label label-danger">' . $this->translation->trans('label.not_accepted') . '</span>';
    }

    protected function getPreviewUrl(int $id): string
    {
        return $this->router->generate(
            'admin_event_details_modal',
            [
                'id' => $id
            ]
        );
    }

    protected function getAcceptUrl(int $id): string
    {
        return $this->router->generate(
            'admin_event_accept',
            [
                'id' => $id
            ]
        );
    }

    protected function getEditUrl(int $id): string
    {
        return $this->router->generate(
            'admin_event_edit',
            [
                'id' => $id
            ]
        );
    }

    protected function getDeleteUrl(int $id): string
    {
        return $this->router->generate(
            'admin_event_remove',
            [
                'id' => $id
            ]
        );
    }
    protected function getEditEntityUrl(int $id): string
    {
        return $this->router->generate(
            'admin_entity_edit',
            [
                'id' => $id
            ]
        );
    }

    protected function createStatusLabel(int $status): string
    {
        if (Event::INCOMING === $status) {
            return '<span class="label label-success">' .
                $this->translation->trans(Event::STATUS[Event::INCOMING])
                . '</span>';
        } elseif (Event::ONGOING === $status) {
            return '<span class="label label-info">' .
                $this->translation->trans(Event::STATUS[Event::ONGOING]) .
                '</span>';
        } elseif (Event::PAST === $status) {
            return '<span class="label label-danger">' .
                $this->translation->trans(Event::STATUS[Event::PAST]) .
                '</span>';
        } else
            return '';
    }

    protected function getFreeEventLabel(bool $isFreeEntry = false): string
    {
        if ($isFreeEntry) {
            return '<span class="label label-primary ml10 mr5 mb10 ib lh15">F</span>';
        }
        return '';
    }

    protected function getCycleEventLabel(bool $isCycleEvent = false): string
    {
        if ($isCycleEvent) {
            return '<span class="label label-system ml10 mr5 mb10 ib lh15">C</span>';
        }
        return '';
    }
}
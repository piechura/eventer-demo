<?php
namespace App\DataTables\Admin;

use App\DataTables\AppDataTableHandler;
use App\Entity\Entity;
use DataTables\DataTableQuery;
use Doctrine\ORM\QueryBuilder;

class EntityAdminDataTable extends AppDataTableHandler
{
    const ID = 'admin_entity_datatable';

    protected $tableId = self::ID;
    protected $masterClass = Entity::class;
    protected $masterAlias = 'e';
    protected $useSession = false;

    protected function getOrderColumns(): array
    {
        return [
            $this->getOrderColumn('id'),
            $this->getOrderColumn('name'),
            $this->getOrderColumn('name'),
            $this->getOrderColumn('createdAt'),
            $this->getOrderColumn('updateDate'),
        ];
    }

    /**
     * @param Entity $entity
     * @return array
     */
    protected function buildResultsRow($entity): array
    {
        return [
            $entity->getId(),
            $entity->getName(),
            $this->getEntityUsersList($entity),
            $this->getDate($entity->getCreatedAt()),
            $this->getDate($entity->getUpdateDate()),
            $this->createActionsButtonWithLinks(
                $this->getActionsDataArray($entity),
                $this->translation->trans('label.actions')
            )
        ];
    }

    protected function setSearchQuery(QueryBuilder $query, DataTableQuery $request): QueryBuilder
    {
        if ($request->search->value != '' && $request->search->value) {
            $query->where('(LOWER(e.name) LIKE :search OR' .
                ' LOWER(e.description) LIKE :search)');
            $query->setParameter(
                'search',
                strtolower("%{$request->search->value}%")
            );
        }
        return $query;
    }

    protected function getActionsDataArray(Entity $entity): array
    {
        return [
            [
                'type' => 'basic',
                'url' => $this->getEditUrl($entity->getId()),
                'label' => $this->translation->trans('action.edit'),
                'target' => '_self'
            ],
            [
                'type' => 'delete-with-confirmation',
                'url' => $this->getDeleteUrl($entity->getId()),
                'label' => $this->translation->trans('action.delete'),
            ],
        ];
    }

    protected function getEntityUsersList($entity)
    {
        $usersList = '';
        foreach ($entity->getUsers() as $user) {
            $usersList .= '<a href="' . $this->router->generate('admin_user_edit', ['id' => $user->getId()]) . '" target="_blank">' . $user->getFullName() . "</a></br>";
        }
        return $usersList;
    }

    protected function getEditUrl(int $id): string
    {
        return $this->router->generate(
            'admin_entity_edit',
            [
                'id' => $id
            ]
        );
    }

    protected function getDeleteUrl(int $id): string
    {
        return $this->router->generate(
            'admin_entity_remove',
            [
                'id' => $id
            ]
        );
    }
}
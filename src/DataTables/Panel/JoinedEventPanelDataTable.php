<?php
namespace App\DataTables\Panel;

use App\DataTables\Admin\EventAdminDataTable;
use App\Entity\Event;
use Doctrine\ORM\QueryBuilder;

class JoinedEventPanelDataTable extends EventAdminDataTable
{
    const ID = 'panel_joined_events_datatable';
    protected $tableId = self::ID;

    protected function initQuery(): QueryBuilder
    {
        $query = parent::initQuery();
        $query
            ->innerJoin('e.participants', 'u')
            ->andWhere('u.id = :loggedUser')
            ->setParameter('loggedUser', $this->security->getUser()->getId());
        return $query;
    }

    protected function getOrderColumns(): array
    {
        return [
            $this->getOrderColumn('id'),
            $this->getOrderColumn('name'),
            $this->getOrderColumn('category'),
            $this->getOrderColumn('entity'),
            $this->getOrderColumn('status'),
            $this->getOrderColumn('startAt'),
            $this->getOrderColumn('endAt'),
        ];
    }

    /**
     * @param Event $entity
     * @return array
     */
    protected function buildResultsRow($entity): array
    {
        return [
            $entity->getId(),
            $entity->getName() . $this->getFreeEventLabel($entity->isFreeEntry()) . $this->getCycleEventLabel($entity->isCycleInterval()),
            $this->getColorLabel($entity->getCategory()->getColor(), $entity->getCategory()->getName()),
            $entity->getEntity()->getName(),
            $this->createStatusLabel($entity->getStatus()),
            $this->getDate($entity->getStartAt(), 'Y-m-d H:i'),
            $this->getDate($entity->getEndAt(), 'Y-m-d H:i'),
            $this->createActionsButtonWithLinks(
                $this->getActionsDataArray($entity),
                $this->translation->trans('label.actions')
            )
        ];
    }

    protected function getActionsDataArray(Event $entity): array
    {
        $actionsDataArray = [];
        if ($entity->getStatus() == Event::INCOMING) {
            $actionsDataArray[] = [
                'type' => 'unjoin-event-action',
                'url' => $this->getUnjoinUrl($entity->getId()),
                'label' => $this->translation->trans('action.unjoin'),
            ];
        }
        return $actionsDataArray;
    }

    protected function getUnjoinUrl(int $id): string
    {
        return $this->router->generate(
            'panel_joined_event_remove',
            [
                'id' => $id
            ]
        );
    }
}
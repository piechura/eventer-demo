<?php
namespace App\DataTables\Panel;

use App\DataTables\Admin\EventAdminDataTable;
use App\Exception\BadRoleException;
use Doctrine\ORM\QueryBuilder;

class EventPanelDataTable extends EventAdminDataTable
{
    const ID = 'panel_events_datatable';

    protected $tableId = self::ID;

    protected function initQuery(): QueryBuilder
    {
        $query = parent::initQuery();
        if ($this->security->isGranted('ROLE_USER')) {
            $query
                ->andWhere($this->getMasterAlias() . '.addedBy = :loggedUser')
                ->setParameter('loggedUser', $this->security->getUser()->getId());
        } else {
            throw new BadRoleException($this->translation->trans('exceptions.bar_role'));
        }
        return $query;
    }

    protected function getPreviewUrl(int $id): string
    {
        return $this->router->generate(
            'panel_event_details_modal',
            [
                'id' => $id
            ]
        );
    }
    protected function getEditUrl(int $id): string
    {
        return $this->router->generate(
            'panel_event_edit',
            [
                'id' => $id
            ]
        );
    }

    protected function getDeleteUrl(int $id): string
    {
        return $this->router->generate(
            'panel_event_remove',
            [
                'id' => $id
            ]
        );
    }
    protected function getEditEntityUrl(int $id): string
    {
        return $this->router->generate(
            'panel_entity_edit',
            [
                'id' => $id
            ]
        );
    }
}
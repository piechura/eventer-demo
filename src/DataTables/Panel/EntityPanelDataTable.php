<?php
namespace App\DataTables\Panel;

use App\DataTables\Admin\EntityAdminDataTable;
use App\Exception\BadRoleException;
use Doctrine\ORM\QueryBuilder;

class EntityPanelDataTable extends EntityAdminDataTable
{
    const ID = 'panel_entity_datatable';
    protected $tableId = self::ID;

    protected function initQuery(): QueryBuilder
    {
        $query = parent::initQuery();
        if ($this->security->isGranted('ROLE_USER')) {
            $query
                ->innerJoin('e.users', 'u')
                ->andWhere('u.id = :loggedUser')
                ->setParameter('loggedUser', $this->security->getUser()->getId());

        } else {
            throw new BadRoleException($this->translation->trans('exceptions.bar_role'));
        }
        return $query;
    }

    protected function getEntityUsersList($entity)
    {
        $usersList = '';
        foreach ($entity->getUsers() as $user) {
            $usersList .= $user->getFullName() . "</br>";
        }
        return $usersList;
    }

    protected function getEditUrl(int $id): string
    {
        return $this->router->generate(
            'panel_entity_edit',
            [
                'id' => $id
            ]
        );
    }

    protected function getDeleteUrl(int $id): string
    {
        return $this->router->generate(
            'panel_entity_remove',
            [
                'id' => $id
            ]
        );
    }
}
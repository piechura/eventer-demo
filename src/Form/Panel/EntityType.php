<?php
namespace App\Form\Panel;

use App\Entity\Entity;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Contracts\Translation\TranslatorInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EntityType extends AbstractType
{
    private $usersChoices = [];
    private $translator;
    public function __construct(UserRepository $userRepository, TranslatorInterface $translator)
    {
        $this->translator = $translator;
        $this->usersChoices = $userRepository->getActiveUsers();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'label' => 'label.name',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.placeholder_name'
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'label' => 'label.description',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.placeholder_description'
                ]
            ])
            ->add('city', TextType::class, [
                'required' => true,
                'label' => 'label.city',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.placeholder_city'
                ]
            ])
            ->add('address', TextType::class, [
                'required' => true,
                'label' => 'label.address',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.placeholder_address'
                ]
            ])
            ->add('contactEmail', EmailType::class, [
                'required' => true,
                'label' => 'label.contact_email',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.placeholder_email'
                ]
            ])
            ->add('contactPhone', TextType::class, [
                'required' => false,
                'label' => 'label.contact_phone',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.placeholder_phone'
                ]
            ])
            ->add('externalLink', TextType::class, [
                'required' => false,
                'label' => 'label.external_link',
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.http',
                ]
            ])
            ->add('logoFile', VichImageType::class, [
                'required' => false,
                'label' => 'label.logo',
                'allow_delete' => true,
                'download_label' => 'label.download',
                'download_uri' => true,
                'image_uri' => true,
                'delete_label' => 'label.delete?',
                'imagine_pattern' => 'entity_logo',
                'asset_helper' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Entity::class,
        ]);
    }
}
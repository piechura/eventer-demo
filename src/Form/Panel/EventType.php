<?php
namespace App\Form\Panel;

use App\Entity\Category;
use App\Entity\Entity;
use App\Entity\Event;
use App\Form\Type\DateTimePickerType;
use App\Repository\CategoryRepository;
use App\Repository\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as BaseEntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Valid;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;


class EventType extends AbstractType
{
    private $activeCategories = [];
    private $activeEntities = [];
    public function __construct(CategoryRepository $categoryRepository, EntityRepository $entityRepository)
    {
        $this->activeCategories = $categoryRepository->getActiveCategories();
        $this->activeEntities = $entityRepository->getActiveEntities();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'disabled' => $options['disabled'],
                'label' => 'label.name',
                'attr' => [
                    'class' => 'gui-input',
                    'autocomplete' => 'disabled',
                    'placeholder' => 'panel_event_form.event_name',
                ]
            ])
            ->add('category', BaseEntityType::class, [
                'class' => Category::class,
                'required' => true,
                'disabled' => $options['disabled'],
                'placeholder' => 'label.select',
                'choices' => $this->activeCategories,
                'choice_label' => 'name',
                'query_builder' => function(CategoryRepository $repo) {
                    return $repo->findActiveCategories();
                },
                'attr' => [
                    'class' => 'select2box'
                ]

            ])
            ->add('startAt', DateTimePickerType::class, [
                'required' => true,
                'label' => 'label.start_at',
                'help' => 'help.post_publication',
                'disabled' => $options['disabled'],
                'format' => DateTimePickerType::HTML5_FORMAT,
                'attr' => [
                    'class' => 'gui-input datetimepicker',
                    'placeholder' => 'panel_event_form.choose_date',
                ]
            ])
            ->add('endAt', DateTimePickerType::class, [
                'required' => true,
                'label' => 'label.end_at',
                'help' => 'help.post_publication',
                'disabled' => $options['disabled'],
                'format' => DateTimePickerType::HTML5_FORMAT,
                'attr' => [
                    'class' => 'gui-input datetimepicker',
                    'placeholder' => 'panel_event_form.choose_date',
                ]
            ])

            ->add('isExistingEntity', ChoiceType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.entity',
                'placeholder' => false,
                'choices' => array_flip(Event::IS_ENTITY),
                'attr' => [
                    'class' => 'mt10'
                ]
            ])
            ->add('entity', BaseEntityType::class, [
                'class' => Entity::class,
                'required' => true,
                'disabled' => $options['disabled'],
                'label' => 'label.entity',
                'placeholder' => 'label.select',
                'choices' => $this->activeEntities,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'select2box'
                ],
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'attr' => [
                    'placeholder' => 'label.placeholder_address',
                    'class' => 'gui-input'
                ]
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'attr' => [
                    'placeholder' => 'label.placeholder_city',
                    'class' => 'gui-input'
                ]
            ])
            ->add('entityAddress', CheckboxType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.the_same_address',
                'attr' => [
                    'class' => 'mt10',
                ]
            ])
            ->add('newEntity', EntityType::class, [
                'required' => false,
                'label' => 'label.entity',
                'attr' => [
                    'class' => 'select2box'
                ],
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.description',
                'attr' => [
                    'class' => 'gui-input summernote-only-text',
                    'placeholder' => 'label.placeholder_description'
                ]
            ])
            ->add('agenda', TextareaType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.agenda',
                'attr' => [
                    'class' => 'gui-input summernote-only-text',
                    'placeholder' => 'label.placeholder_agenda'
                ]
            ])
            ->add('externalLink', TextareaType::class, [
                'required' => false,
                'label' => 'label.external_link',
                'disabled' => $options['disabled'],
                'attr' => [
                    'placeholder' => 'label.http',
                    'class' => 'gui-input',
                ]
            ])
            ->add('tickets', TextareaType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.tickets',
                'attr' => [
                    'class' => 'gui-input summernote-only-text',
                    'placeholder' => 'label.placeholder_tickets'
                ]
            ])
            ->add('freeEntry', CheckboxType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.is_event_free',
                'attr' => [
                    'class' => 'gui-input'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.image',
                'allow_delete' => $options['disabled'] ? false : true,
                'download_label' => 'label.download',
                'download_uri' => $options['disabled'] ? false : true,
                'image_uri' => true,
                'delete_label' => 'label.delete?',
                'imagine_pattern' => 'event_image_thumbnail',
                'asset_helper' => false,
                'attr' => [
                    'class' => 'gui-input',
                ]
            ])
        ;

        if ($options['type'] === 'adding') {
            $builder
                ->add('interval', ChoiceType::class, [
                    'required' => false,
                    'disabled' => $options['disabled'],
                    'label' => 'label.interval',
                    'placeholder' => 'label.select',
                    'choices' => array_flip(Event::INTERVAL_VISIT),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('cycle', ChoiceType::class, [
                    'required' => false,
                    'disabled' => $options['disabled'],
                    'placeholder' => 'label.select',
                    'label' => 'label.cycle',
                    'choices' => array_flip(Event::CYCLE_VISIT),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ;
        }

        $builder
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $eventEntity = $event->getData();
                $form = $event->getForm();

                if ($eventEntity['isExistingEntity'] === '0') {
                    $form
                        ->add('entity', BaseEntityType::class, [
                            'class' => Entity::class,
                            'required' => false,
                            'label' => 'label.entity',
                            'placeholder' => 'label.select',
                            'choices' => $this->activeEntities,
                            'choice_label' => 'name',
                            'attr' => [
                                'class' => 'select2box'
                                ],
                        ])
                        ->add('newEntity', EntityType::class, [
                            'required' => true,
                            'label' => 'label.entity',
                            'attr' => [
                                'class' => 'select2box'
                            ],
                            'constraints' => new Valid(),
                        ])
                    ;
                }
        });



    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'format' => 'YYYY-MM-DD hh:mm',
            'disabled' => false,
            'type' => 'editing'
        ]);
    }
}
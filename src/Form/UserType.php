<?php

namespace App\Form;

use App\Entity\Entity;
use App\Entity\User;
use App\Repository\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as BaseEntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserType extends AbstractType
{
    private $translator;
    private $entitiesChoices;

    public function __construct(TranslatorInterface $translator, EntityRepository $entityRepository)
    {
        $this->translator = $translator;
        $this->entitiesChoices = $entityRepository->getActiveEntities();
    }

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('fullName', TextType::class, [
                'label' => 'label.fullname',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.email',
                'disabled' => $options['disableEmail'],
                'attr' => [
                    'class' => 'form-control'
                ]
            ]);

        if ($options['hideRoles'] === false) {
            $builder
                ->add('roles', ChoiceType::class, [
                'label' => 'label.roles',
                'choices'  => User::ROLES,
                'multiple' => true,
                'placeholder' => $this->translator->trans('label.select'),
                'attr' => [
                    'class' => 'multiselect'
                ]
            ]);
        }
        if ($options['hideIsActive'] === false) {
            $builder
                ->add('is_active', CheckboxType::class, [
                    'label' => 'label.isActive',
                    'required' => false,
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ]);
        }
        if ($options['hideEntities'] === false) {
            $builder
            ->add('entities', BaseEntityType::class, [
                'label' => 'label.entities',
                'required' => false,
                'by_reference' => false,
                'class' => Entity::class,
                'multiple' => true,
                'choice_label' => 'name',
                'choices' => $this->entitiesChoices,
                'attr' => [
                    'class' => 'form-control dualListBox',
                    'data-nonSelectedListLabel' => $this->translator->trans('label.nonSelectedListLabel'),
                    'data-selectedListLabel' => $this->translator->trans('label.selectedListLabel')
                ]
            ]);
        }



    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'hideRoles' => false,
            'hideIsActive' => false,
            'hideEntities' => false,
            'disableEmail' => true,
        ]);
    }
}

<?php

namespace App\Form\Front;

use App\Entity\Event;
use App\Repository\CategoryRepository;
use App\Repository\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class CalendarFrontType extends AbstractType
{
    private $translator;
    private $categoryChoices;
    private $entityChoices;

    public function __construct(
        TranslatorInterface $translator,
        CategoryRepository $categoryRepository,
        EntityRepository $entityRepository
    ) {
        $this->translator = $translator;
        $this->categoryChoices = $categoryRepository->getActiveCategoriesChoices();
        $this->entityChoices = $entityRepository->getActiveEntitiesChoices();
    }

    public function getName()
    {
        return self::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category', ChoiceType::class, [
                'label' => false,
                'placeholder' => $this->translator->trans('label.category') . '...',
                'choices' => array_flip($this->categoryChoices),
                'attr' => [
                    'class' => 'form-control select2-single',
                ]
            ])
            ->add('entity', ChoiceType::class, [
                    'required' => false,
                    'label' => false,
                    'placeholder' =>  $this->translator->trans('label.entity') . '...',
                    'choices' => array_flip($this->entityChoices),
                    'attr' => [
                        'class' => 'form-control select2-single',
                    ]
                ]
            )
            ->add('status', ChoiceType::class, [
                    'required' => false,
                    'label' => false,
                    'placeholder' =>  $this->translator->trans('label.status') . '...',
                    'choices' => array_flip(Event::STATUS),
                    'attr' => [
                        'class' => 'form-control select2-single',
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null
        ]);
    }
}
<?php

namespace App\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class NewPasswordType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('newPassword', RepeatedType::class, [
                'type' => PasswordType::class,
                'constraints' => [
                    new NotBlank(),
                    new Length([
                        'min' => 5,
                        'max' => 128,
                    ]),
                ],
                'first_options' => [
                    'label' => 'label.new_password',
                    'attr' => [
                        'autocomplete' => 'off',
                        'class' => 'form-control',
                        'placeholder' => 'label.write',
                    ],
                ],
                'second_options' => [
                    'label' => 'label.new_password_confirm',
                    'attr' => [
                        'autocomplete' => 'off',
                        'class' => 'form-control',
                        'placeholder' => 'label.write',
                    ],
                ],
            ])
        ;
    }
}

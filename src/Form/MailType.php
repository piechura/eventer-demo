<?php
namespace App\Form;

use App\Entity\Mail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;


class MailType extends AbstractType
{
    private $params;

    public function __construct(ParameterBagInterface $params, array $options = [])
    {
        $this->params = $params;
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fromEmail', EmailType::class, [
                'required' => true,
                'data' => $this->params->get('notification_from_email'),
                'disabled' => true,
                'attr' => [
                    'readonly' => true,
                ]
            ])
            ->add('fromName', TextType::class, [
                'required' => true,
                'data' => $this->params->get('notification_from_name'),
                'disabled' => true,
                'attr' => [
                    'readonly' => true,
                ]
            ])
            ->add('toEmail', TextareaType::class, [
                'required' => true,
                'data' => $options['toEmail'],
                'attr' => [
                    'readonly' => true,
                ]
            ])
            ->add('subject', TextType::class, [
                'required' => true,
                'data' => $options['subject'] ?? '',
            ])
            ->add('type', TextType::class, [
                'required' => true,
                'disabled' => true,
                'data' => $options['type'] ?? '',
            ])
            ->add('body', TextareaType::class, [
                'required' => true,
                'data' => $options['body'] ?? '',
            ])
            ->add('attachments', CollectionType::class, [
                'entry_type' => MailAttachmentsType::class,
                'mapped' => false,
                'entry_options' => ['label' => 'Załączniki'],
                'prototype'			=> true,
                'allow_add'			=> true,
                'allow_delete'		=> true,
                'by_reference' 		=> false,
                'required'			=> false,
                'label'				=> false,
                'attr'         => [
                    'class' => 'collection',
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired([
            'toEmail',
            'body',
            'subject',
            'type',
        ]);

        $resolver->setDefaults([
            'data_class' => Mail::class,
            'csrf_protection' => false,
            'toEmail' => '',
            'body' => '',
            'subject' => '',
            'type' => '',
        ]);
    }
}
<?php
namespace App\Form;

use App\Entity\Category;
use App\Entity\Entity;
use App\Entity\Event;
use App\Form\Type\DateTimePickerType;
use App\Repository\CategoryRepository;
use App\Repository\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType as BaseEntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EventType extends AbstractType
{
    private $activeCategories = [];
    private $activeEntities = [];
    public function __construct(CategoryRepository $categoryRepository, EntityRepository $entityRepository)
    {
        $this->activeCategories = $categoryRepository->getActiveCategories();
        $this->activeEntities = $entityRepository->getActiveEntities();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'required' => true,
                'disabled' => $options['disabled'],
                'label' => 'label.name',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('description', TextareaType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.description',
                'attr' => [
                    'class' => 'form-control summernote-only-text'
                ]
            ])
            ->add('agenda', TextareaType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.agenda',
                'attr' => [
                    'class' => 'form-control summernote-only-text'
                ]
            ])
            ->add('category', BaseEntityType::class, [
                'class' => Category::class,
                'required' => true,
                'disabled' => $options['disabled'],
                'placeholder' => 'label.select',
                'choices' => $this->activeCategories,
                'choice_label' => 'name',
                'query_builder' => function(CategoryRepository $repo) {
                    return $repo->findActiveCategories();
                },
                'attr' => [
                    'class' => 'form-control select2-single',
                ]
            ])
            ->add('entity', BaseEntityType::class, [
                'class' => Entity::class,
                'required' => true,
                'disabled' => $options['disabled'],
                'label' => 'label.entity',
                'placeholder' => 'label.select',
                'choices' => $this->activeEntities,
                'choice_label' => 'name',
                'attr' => [
                    'class' => 'form-control select2-single',
                    'ng-required' => true,
                ]
            ])
            ->add('address', TextType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'attr' => [
                    'placeholder' => 'label.street_address',
                    'class' => 'form-control'
                ]
            ])
            ->add('city', TextType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'attr' => [
                    'placeholder' => 'label.city',
                    'class' => 'form-control'
                ]
            ])
            ->add('entityAddress', CheckboxType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.the_same_address',
                'attr' => [
                    'class' => 'mt10'
                ]
            ])
            ->add('startAt', DateTimePickerType::class, [
                'required' => true,
                'label' => 'label.start_at',
                'help' => 'help.post_publication',
                'disabled' => $options['disabled'],
                'format' => DateTimePickerType::HTML5_FORMAT,
                'attr' => [
                    'class' => 'form-control datetimepicker'
                ]
            ])
            ->add('endAt', DateTimePickerType::class, [
                'required' => true,
                'label' => 'label.end_at',
                'help' => 'help.post_publication',
                'disabled' => $options['disabled'],
                'format' => DateTimePickerType::HTML5_FORMAT,
                'attr' => [
                    'class' => 'form-control datetimepicker',
                ]
            ])
            ->add('externalLink', TextType::class, [
                'required' => false,
                'label' => 'label.external_link',
                'disabled' => $options['disabled'],
                'attr' => [
                    'class' => 'form-control',
                    'placeholder' => 'label.http',
                ]
            ])
            ->add('tickets', TextareaType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.tickets',
                'attr' => [
                    'class' => 'form-control summernote-only-text'
                ]
            ])
            ->add('freeEntry', CheckboxType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.free_entry',
                'attr' => [
                    'class' => 'form-control'
                ]
            ])
            ->add('imageFile', VichImageType::class, [
                'required' => false,
                'disabled' => $options['disabled'],
                'label' => 'label.image',
                'allow_delete' => $options['disabled'] ? false : true,
                'download_label' => 'label.download',
                'download_uri' => $options['disabled'] ? false : true,
                'image_uri' => true,
                'delete_label' => 'label.delete?',
                'imagine_pattern' => 'event_image_thumbnail',
                'asset_helper' => false,
                'attr' => [
                    'class' => 'form-control',
                ]
            ])

        ;

        if ($options['type'] === 'adding') {
            $builder
                ->add('interval', ChoiceType::class, [
                    'required' => false,
                    'disabled' => $options['disabled'],
                    'label' => 'label.interval',
                    'placeholder' => 'label.select',
                    'choices' => array_flip(Event::INTERVAL_VISIT),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
                ->add('cycle', ChoiceType::class, [
                    'required' => false,
                    'disabled' => $options['disabled'],
                    'placeholder' => 'label.select',
                    'label' => 'label.cycle',
                    'choices' => array_flip(Event::CYCLE_VISIT),
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ;
        }

        if ($options['type'] === 'editing') {
            $builder
                ->add('accepted', CheckboxType::class, [
                    'required' => false,
                    'disabled' => $options['disabled'],
                    'label' => 'label.accepted',
                    'attr' => [
                        'class' => 'form-control'
                    ]
                ])
            ;
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'format' => 'YYYY-MM-DD hh:mm',
            'disabled' => false,
            'type' => 'editing'
        ]);
    }
}
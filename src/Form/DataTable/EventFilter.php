<?php

namespace App\Form\DataTable;

use App\Entity\Event;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class EventFilter extends AbstractType
{
    private $translator;
    private $usersChoices;
    private $categoryChoices;

    public function __construct(TranslatorInterface $translator, UserRepository $userRepository, CategoryRepository $categoryRepository)
    {
        $this->translator = $translator;
        $this->usersChoices = $userRepository->getActiveUsersChoices();
        $this->categoryChoices = $categoryRepository->getActiveCategoriesChoices();
    }

    public function getName()
    {
        return self::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                    'label' => $this->translator->trans('label.name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('label.write'),
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('category', ChoiceType::class, [
                'label' => $this->translator->trans('label.category'),
                'placeholder' => 'label.select',
                'choices' => array_flip($this->categoryChoices),
                'attr' => [
                    'class' => 'form-control select2-single',
                ]
            ])
            ->add('accepted', ChoiceType::class, [
                    'required' => false,
                    'label' => $this->translator->trans('label.acceptance'),
                    'placeholder' => $this->translator->trans('label.select'),
                    'choices' => array_flip(Event::ACCEPTED),
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('status', ChoiceType::class, [
                'required' => false,
                'label' => $this->translator->trans('label.status'),
                'placeholder' => $this->translator->trans('label.select'),
                'choices' => array_flip(Event::STATUS),
                'attr' => [
                    'class' => 'form-control',
                    ]
                ]
            );
        if ($options['hideAddedBy'] == false) {
            $builder
                ->add('addedBy', ChoiceType::class, [
                    'label' => $this->translator->trans('label.added_by'),
                    'placeholder' => 'label.select',
                    'choices' => array_flip($this->usersChoices),
                    'attr' => [
                        'class' => 'form-control select2-single',
                    ]
                ]);
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
            'hideAddedBy' => false,
        ]);
    }
}
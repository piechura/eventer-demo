<?php

namespace App\Form\DataTable;

use App\Entity\User;
use App\Repository\CategoryRepository;
use App\Repository\EntityRepository;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserFilter extends AbstractType
{
    private $translator;
    private $usersChoices;
    private $categoryChoices;
    private $entityChoices;

    public function __construct(
        TranslatorInterface $translator,
        UserRepository $userRepository,
        CategoryRepository $categoryRepository,
        EntityRepository $entityRepository
    ) {
        $this->translator = $translator;
        $this->usersChoices = $userRepository->getActiveUsersChoices();
        $this->categoryChoices = $categoryRepository->getActiveCategoriesChoices();
        $this->entityChoices = $entityRepository->getActiveEntitiesChoices();
    }

    public function getName()
    {
        return self::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                    'label' => $this->translator->trans('label.name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('label.write'),
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('email', TextType::class, [
                    'label' => $this->translator->trans('label.email'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('label.write'),
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('roles', ChoiceType::class, [
                'label' => $this->translator->trans('label.roles'),
                'placeholder' => 'label.select',
                'choices' => User::ROLES,
                'attr' => [
                    'class' => 'form-control select2-single',
                    ]
                ]
            )
            ->add('activity', ChoiceType::class, [
                    'required' => false,
                    'label' => $this->translator->trans('label.activity'),
                    'placeholder' => $this->translator->trans('label.select'),
                    'choices' => array_flip(User::ACTIVITY),
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('entity', ChoiceType::class, [
                'required' => false,
                'label' => $this->translator->trans('label.entity'),
                'placeholder' => $this->translator->trans('label.select'),
                'choices' => array_flip($this->entityChoices),
                'attr' => [
                    'class' => 'form-control select2-single',
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
<?php

namespace App\Form\DataTable;

use App\Entity\Mail;
use App\Repository\MailRepository;
use App\Repository\UserRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

class MessageFilter extends AbstractType
{
    private $translator;
    private $usersChoices;
    private $typesChoices;

    public function __construct(TranslatorInterface $translator, UserRepository $userRepository, MailRepository $mailRepository)
    {
        $this->translator = $translator;
        $this->usersChoices = $userRepository->getActiveUsersChoices();
        $this->typesChoices = $mailRepository->findUniqueMessagesTypesChoices();
    }

    public function getName()
    {
        return self::class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('userId', ChoiceType::class, [
                'required' => false,
                'label' => $this->translator->trans('label.user'),
                'placeholder' => $this->translator->trans('label.select'),
                'choices' => array_flip($this->usersChoices),
                'attr' => [
                    'class' => 'form-control select2-single',
                    ]
                ]
            )
            ->add('toEmail', TextType::class, [
                    'required' => false,
                    'label' => $this->translator->trans('label.email'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('label.write'),
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('fullName', TextType::class, [
                    'required' => false,
                    'label' => $this->translator->trans('label.name'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('label.write'),
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('isSent', ChoiceType::class, [
                    'required' => false,
                    'label' => $this->translator->trans('label.status'),
                    'placeholder' => $this->translator->trans('label.select'),
                    'choices'  => array_flip(Mail::STATUS),
                    'choice_translation_domain' => true,
                    'attr' => [
                        'class' => 'form-control',
                    ]
                ]
            )
            ->add('type', ChoiceType::class, [
                'required' => false,
                'label' => $this->translator->trans('label.type'),
                'placeholder' => $this->translator->trans('label.select'),
                'choices' => $this->typesChoices,
                'attr' => [
                    'class' => 'form-control select2-single',
                    ]
                ]
            )
            ->add('subject', TextType::class, [
                    'required' => false,
                    'label' => $this->translator->trans('label.title'),
                    'attr' => [
                        'placeholder' => $this->translator->trans('label.write'),
                        'class' => 'form-control',
                    ]
                ]
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => null,
        ]);
    }
}
# Eventer Demo


## IMPORTNT: Here are samples of code, this is not entire project, you can not set up working project with these files.
An application for creating, sharing events related with entities.


## Requirements

  * PHP 7.2.9 or higher;
  * PDO-SQLite PHP extension enabled;
  * and the [usual Symfony application requirements](https://symfony.com/doc/4.2/reference/requirements.html).
* node.js


## Tech stack
 * PHP 7.4 with Symfony 5.1
 * jQuery
 * Webpack


## Features
 * OAuth2 Facebook, Google
 * XSS Protection
 * Clickjacking Protection
 * Translations
 * DataTables
 * FullCalendar
 * Google Maps API



## Installation

1 Clone [repository](https://piechura@bitbucket.org/piechura/ev1.git) using [git](https://getcomposer.org/download/):

```bash
git clone https://piechura@bitbucket.org/piechura/ev1.git
```

2 Run composer

```bash
compser install
```

3 Set up database connection. Create and edit .env file from .env.dist

4 Create database and doctrine schema:

```bash
php bin/console doctrine:database:create
php bin/console doctrine:schema:update -f
```

5 Generate json routing and translations for JS:

```bash
php bin/console fos:js-routing:dump --format=json --target=public/js/fos_js_routes.json
php bin/console bazinga:js-translation:dump --format=js --merge-domains assets/js/bazinga
```

6 Run Yarn
```bash
yarn
yarn dev
```

7 Fixtures:


```bash
php bin/console doctrine:fixtures:load --append
```


8 Tests
```bash
php bin/phpunit 
php bin/phpunit tests/Controller
```



## License
Piotr Piechura Copyright ©. All right reserved.